r-VEX binutils port
===================

This repository is derived from GNU binutils-gdb, with the r-VEX target added
to it.


Contents
--------

 - The r-VEX assembler (`rvex-elf32-as`).
 - The r-VEX linker (`rvex-elf32-ld`).
 - ELF manipulation tools (`rvex-elf32-objcopy`, `rvex-elf32-objdump` etc.).
 - Library manipulation tools (`rvex-elf32-ar`, `rvex-elf32-ranlib`).
 - GDB (`rvex-elf32-gdb`).

**WARNING:** GDB support is rather unstable; we never really use it because
there are problems with debug symbol generation. Basically, you can only set
breakpoints at function entry points; line numbers (and therefore stepping
through C code) don't work right. Nevertheless, you can use it in conjunction
with `rvd` (from the rvex-debug repository). Run `rvd gdb` for more information.


Configuration and build process
-------------------------------

Configuring and building `binutils` works like this:

 - `rvex-cfg.py <json>`. This stage configures the *default* linker script and
   assembler flags that set up the core configuration; the tools remain
   completely command-line retargetable. This is automatically run by the r-VEX
   toolchain generator, in which case the JSON file is generated based on a
   user-friendly INI file. If you're building manually, you can run
   `rvex-cfg.py -` instead to generate the required files with default values.
 - `configure --target=rvex-elf32 --program-prefix=rvex-elf32-`. Generates a
   Makefile for the later steps. Allows you to set a custom build directory
   (by running the `configure` command from another working directory, and a
   custom installation directory using `--prefix` (strongly recommended; you
   probably don't want to install the r-VEX compiler in your Linux root).
 - `make`. Compiles the tools.
 - `make install`. Copies the generated files to the specified `install`
   directory.

