#include "sysdep.h"
#include "bfd.h"
#include "libbfd.h"

const bfd_arch_info_type bfd_rvex_arch = 
{
	32,
	32, //check this
	8,
	bfd_arch_rvex,
	bfd_mach_rvex,
	"rvex",
	"rvex",
	3,
	TRUE,
	bfd_default_compatible,
	bfd_default_scan,
    bfd_arch_default_fill,
	0
};

