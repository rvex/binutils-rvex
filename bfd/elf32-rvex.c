/*adapted from elf32-cr16.c*/

#include "sysdep.h"
#include "bfd.h"
#include "bfdlink.h"
#include "libbfd.h"
#include "libiberty.h"
#include "elf-bfd.h"
#include "elf/rvex.h"
#include "opcode/rvex.h"

/* rvex_reloc_map array maps BFD relocation enum into a CRGAS relocation type.  */

struct rvex_reloc_map
{
  bfd_reloc_code_real_type bfd_reloc_enum; /* BFD relocation enum.  */
  unsigned short rvex_reloc_type;          /* RVEX relocation type.  */
};

static const struct rvex_reloc_map rvex_reloc_map[R_RVEX_MAX] =
{
  {BFD_RELOC_NONE,           R_RVEX_NONE},
  {BFD_RELOC_32,			R_RVEX_32},
  {BFD_RELOC_RVEX_BRANCH,      R_RVEX_BRANCH},
  {BFD_RELOC_RVEX_MOV,      R_RVEX_MOV},
  {BFD_RELOC_RVEX_MOV_H, R_RVEX_MOV_H},
  {BFD_RELOC_RVEX_MOV_L, R_RVEX_MOV_L},
};

static reloc_howto_type rvex_elf_howto_table[] =
{
  HOWTO (R_RVEX_NONE,              /* type */
         0,                        /* rightshift */
         2,                        /* size */
         32,                       /* bitsize */
         FALSE,                    /* pc_relative */
         0,                        /* bitpos */
         complain_overflow_dont,   /* complain_on_overflow */
         bfd_elf_generic_reloc,    /* special_function */
         "R_RVEX_NONE",            /* name */
         FALSE,                    /* partial_inplace */
         0,                        /* src_mask */
         0,                        /* dst_mask */
         FALSE),                   /* pcrel_offset */

  HOWTO (R_RVEX_BRANCH,              /* type */
         (2+LOG_MIN_WIDTH),          /* rightshift */
         2,                          /* size */
         19,                         /* bitsize */
         TRUE,                       /* pc_relative */
         5,                          /* bitpos */
         complain_overflow_signed,   /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_RVEX_BRANCH",            /* name */
         FALSE,                      /* partial_inplace */
         0x0,                        /* src_mask */
         0xffffe0,                   /* dst_mask */
         TRUE),                      /* pcrel_offset */

  HOWTO (R_RVEX_MOV,                 /* type */
         0,                          /* rightshift */
         2,                          /* size */
         9,                          /* bitsize */
         FALSE,                      /* pc_relative */
         2,                          /* bitpos */
         complain_overflow_bitfield, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_RVEX_MOV",               /* name */
         FALSE,                      /* partial_inplace */
         0x7fc,                      /* src_mask */
         0x7fc,                      /* dst_mask */
         FALSE),                     /* pcrel_offset */

  HOWTO (R_RVEX_MOV_H,               /* type */
         9,                          /* rightshift */
         2,                          /* size */
         32,                         /* bitsize */
         FALSE,                      /* pc_relative */
         2,                          /* bitpos */
         complain_overflow_bitfield, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_RVEX_MOV_H",             /* name */
         FALSE,                      /* partial_inplace */
         0x0,                        /* src_mask */
         0x01fffffc,                 /* dst_mask */
         FALSE),                     /* pcrel_offset */

  HOWTO (R_RVEX_MOV_L,               /* type */
         0,                          /* rightshift */
         2,                          /* size */
         32,                         /* bitsize */
         FALSE,                      /* pc_relative */
         2,                          /* bitpos */
         complain_overflow_bitfield, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_RVEX_MOV_L",             /* name */
         FALSE,                      /* partial_inplace */
         0x0,                        /* src_mask */
         0x7fc,                      /* dst_mask */
         FALSE),                     /* pcrel_offset */
         
  HOWTO (R_RVEX_32,                  /* type */
         0,                          /* rightshift */
         2,                          /* size */
         32,                         /* bitsize */
         FALSE,                      /* pc_relative */
         0,                          /* bitpos */
         complain_overflow_bitfield, /* complain_on_overflow */
         bfd_elf_generic_reloc,      /* special_function */
         "R_RVEX_32",                /* name */
         FALSE,                      /* partial_inplace */
         0x0,                        /* src_mask */
         0xffffffff,                 /* dst_mask */
         FALSE),                     /* pcrel_offset */
};


/* Retrieve a howto ptr using a BFD reloc_code.  */

static reloc_howto_type *
elf_rvex_reloc_type_lookup (bfd *abfd ATTRIBUTE_UNUSED,
                            bfd_reloc_code_real_type code)
{
  unsigned int i;

  for (i = 0; i < R_RVEX_MAX; i++)
    if (code == rvex_reloc_map[i].bfd_reloc_enum)
      return &rvex_elf_howto_table[rvex_reloc_map[i].rvex_reloc_type];

  _bfd_error_handler ("Unsupported RVEX relocation type: 0x%x\n", code);
  return NULL;
}

static reloc_howto_type *
elf_rvex_reloc_name_lookup (bfd *abfd ATTRIBUTE_UNUSED,
                            const char *r_name)
{
  unsigned int i;

  for (i = 0; ARRAY_SIZE (rvex_elf_howto_table); i++)
    if (rvex_elf_howto_table[i].name != NULL
        && strcasecmp (rvex_elf_howto_table[i].name, r_name) == 0)
      return rvex_elf_howto_table + i;

  return NULL;
}

/* Retrieve a howto ptr using an internal relocation entry.  */

static void
elf_rvex_info_to_howto (bfd *abfd ATTRIBUTE_UNUSED, arelent *cache_ptr,
                        Elf_Internal_Rela *dst)
{
  unsigned int r_type = ELF32_R_TYPE (dst->r_info);

  BFD_ASSERT (r_type < (unsigned int) R_RVEX_MAX);
  cache_ptr->howto = &rvex_elf_howto_table[r_type];
}

/*
 * rvex stub for fixing large jumps:
 * L1:
 * add r0.1 = r0.1, -16
 * ;;
 * stw 12[r0.1] = l0.0
 * mov l0.0 = TARGET
 * ;;
 * ldw l0.0 = 12[r0.1]
 * return r0.1 = r0.1, 16, l0.0
 * ;;
 * We use return instead of goto so we can pop the stack, perform a branch,
 * and restore the value of the link register all in one cycle (on a 2-issue).
 */

static unsigned int stub_data[] = {
	0x62820fc0,
	0x60000002,
	0x0e800830,
	0x60000000,
	0x0b800000,
	0x84000002,
	0x0d800830,
	0x26000202
};

static bfd_boolean
global_reloc_wrapper(bfd *abfd,
		sec_ptr input_section,
		struct bfd_link_info *link_info,
		Elf_Internal_Rela *rel,
		struct elf_link_hash_entry **hash,
		bfd_vma *relocation)
{
	struct elf_link_hash_entry *h;
	asection *sec;
	struct elf_link_hash_entry **sym_hashes;
	Elf_Internal_Shdr *symtab_hdr;
	*hash = NULL;
	h = NULL;
	sec = NULL;
	symtab_hdr = &elf_tdata (abfd)->symtab_hdr;
	sym_hashes = elf_sym_hashes (abfd);
	unsigned long r_symndx = ELF32_R_SYM (rel->r_info);
	bfd_boolean unresolved_reloc, warned, ignored;
	RELOC_FOR_GLOBAL_SYMBOL (link_info, abfd, input_section, rel,
			r_symndx, symtab_hdr, sym_hashes,
			h, sec, *relocation,
			unresolved_reloc, warned, ignored);
	*hash = h;
	return TRUE;
}


static Elf_Internal_Rela *
get_relocs (asection *sec, int count)
{
	Elf_Internal_Rela *relocs;
	struct bfd_elf_section_data *elfsec_data;

	elfsec_data = elf_section_data (sec);
	relocs = elfsec_data->relocs;
	if (relocs == NULL)
	{
		bfd_size_type relsize;
		relsize = sec->reloc_count * sizeof (*relocs);
		relocs = bfd_alloc (sec->owner, relsize);
		if (relocs == NULL)
			return NULL;
		elfsec_data->relocs = relocs;
		elfsec_data->rela.hdr = bfd_zalloc (sec->owner,
				sizeof (Elf_Internal_Shdr));
		if (elfsec_data->rela.hdr == NULL)
			return NULL;
		elfsec_data->rela.hdr->sh_size = (sec->reloc_count
				* sizeof (Elf32_External_Rela));
		elfsec_data->rela.hdr->sh_entsize = sizeof (Elf32_External_Rela);
		sec->reloc_count = 0;
	}
	relocs += sec->reloc_count;
	sec->reloc_count += count;
	return relocs;
}

/**
 * Get the number of local symbols in abfd.
 */
static unsigned long get_num_local_syms(bfd *abfd)
{
	Elf_Internal_Shdr *symtab_hdr;
	unsigned long num_local_syms;
	symtab_hdr = & elf_tdata (abfd)->symtab_hdr;
	num_local_syms = symtab_hdr->sh_info;
	return num_local_syms;
}

/**
 * Get the total number of global symbols in the symbol table of abfd.
 */
static bfd_size_type get_num_global_syms(bfd *abfd)
{
	Elf_Internal_Shdr *symtab_hdr;
	bfd_size_type num_global_syms;
	symtab_hdr = &elf_tdata (abfd)->symtab_hdr;
	num_global_syms = symtab_hdr->sh_size / sizeof (Elf32_External_Sym);
	num_global_syms -= get_num_local_syms(abfd);
	return num_global_syms;
}

typedef struct stub_entry {
    sec_ptr input_section;
    sec_ptr stub_section;
    Elf_Internal_Rela **relocs;
    long int reloc_count;
	int *h_index;
    struct elf_link_hash_entry **h;
    long int h_count;
    struct stub_entry *next;
} stub_entry;

typedef struct extra_sym_entry {
	bfd *abfd;
	unsigned long count;
	struct extra_sym_entry *next;
} extra_sym_entry;

static extra_sym_entry *extra_sym_list = NULL;

static stub_entry *stub_list = NULL;

/**
 * Find the table extra symbol entry struct for abfd.
 * A new one is created and returned if an existing one is not found.
 * These structs record the number of extra symbols needed for stubs.
 */
static extra_sym_entry *get_extra_sym_entry(bfd *abfd)
{
    extra_sym_entry *entry;
	for (entry = extra_sym_list; entry != NULL; entry = entry->next) {
		if (entry->abfd == abfd)
			return entry;
	}
    entry = bfd_malloc(sizeof(extra_sym_entry));
	if (!entry)
		return NULL;
    entry->abfd = abfd;
    entry->count = 0;
    entry->next = extra_sym_list;
    extra_sym_list = entry;
    return entry;
}

static bfd_boolean add_extra_sym(bfd *abfd)
{
	extra_sym_entry *entry;
	entry = get_extra_sym_entry(abfd);
	if (!entry)
		return FALSE;
	entry->count++;
	return TRUE;
}

/**
 * Return the number of extra symbols that will have to be inserted for all
 * the stubs that will be created.
 */
static unsigned long get_extra_sym_count(bfd *abfd)
{
    extra_sym_entry *entry;
	for (entry = extra_sym_list; entry != NULL; entry = entry->next) {
		if (entry->abfd == abfd)
			return entry->count;
	}
	return 0;
}

/**
 * Add a hash to the sym_hashes table.
 * return TRUE on succes
 * If symindx is not NULL, set the value it points to to the new hash index of
 * the hash.
 */
static bfd_boolean
sym_hashes_add_hash(bfd *input_bfd, struct elf_link_hash_entry *new_hash,
		unsigned long int *symindx)
{
	/*
	 * This is more or less copied from the elf32-sh-symbian relocate_section
	 * function.
	 */
	struct elf_link_hash_entry **sym_hashes;
	bfd_size_type num_global_syms;
	unsigned long num_local_syms;

	unsigned long extra_syms = get_extra_sym_count(input_bfd);

	BFD_ASSERT (! elf_bad_symtab (input_bfd));

	num_local_syms = get_num_local_syms(input_bfd);
	num_global_syms = get_num_global_syms(input_bfd);
	sym_hashes       = elf_sym_hashes (input_bfd);

	struct elf_link_hash_entry **  new_sym_hashes;

	/* This is not very efficient, but it works.  */
	++extra_syms;
	new_sym_hashes = bfd_alloc (input_bfd, (num_global_syms + extra_syms) * sizeof (* sym_hashes ));
	if (new_sym_hashes == NULL)
	{
		return FALSE;
	}
	memcpy (new_sym_hashes, sym_hashes, (num_global_syms + extra_syms - 1) * sizeof * sym_hashes);
	new_sym_hashes[num_global_syms + extra_syms - 1] = new_hash;
	elf_sym_hashes (input_bfd) = sym_hashes = new_sym_hashes;
	if (!add_extra_sym(input_bfd))
		printf("error adding extra sym\n");
	if (symindx != NULL)
		*symindx = num_global_syms + num_local_syms + extra_syms - 1;
	return TRUE;
}

/**
 * Find the stub entry associated with input_section.
 */
static stub_entry *find_stub_entry(sec_ptr input_section)
{
    stub_entry *entry;
    for (entry = stub_list; entry != NULL; entry = entry->next) {
        if (entry->input_section == input_section)
            return entry;
    }
    return NULL;
}

/**
 * Try to find the stub_entry associated with input_section,
 * if none is found create and insert a new one into the list
 * and return a pointer to it.,
 */
static stub_entry *get_stub_entry(sec_ptr input_section)
{
	stub_entry *entry;
	entry = find_stub_entry(input_section);
	if (entry)
		return entry;
	entry = bfd_malloc(sizeof(stub_entry));
	entry->input_section = input_section;
	entry->stub_section = NULL;
	entry->relocs = NULL;
	entry->h_index = NULL;
	entry->reloc_count = 0;
	entry->h = NULL;
	entry->h_count = 0;
	entry->next = stub_list;
	stub_list = entry;
	return entry;
}

/**
 * Checks if there is already a stub entry for relocation rel.
 * Return true if there is, else returns false.
 */
static bfd_boolean stub_check_reloc(stub_entry *entry, Elf_Internal_Rela *rel)
{
	int i;
	for (i = 0; i < entry->reloc_count; i++) {
		if (entry->relocs[i] == rel)
			return TRUE;
	}
	return FALSE;
}


/**
 * Return the index of the link_hash_entry h in the table for stub_entry entry.
 * Returns -1 if the entry is not found.
 */
static int stub_find_hash_entry(stub_entry *entry, struct elf_link_hash_entry *h)
{
	int i;
	for (i = 0; i < entry->h_count; i++) {
		if (entry->h[i] == h)
			return i;
	}
	return -1;
}

/**
 * Add link_hash_entry h to stub_entry.
 * The link_hash_entry are entries pointing at symbols we need
 * to for the relocations in this stub.
 */
static bfd_boolean stub_add_hash_entry(stub_entry *entry, struct elf_link_hash_entry *h)
{
	int i;
	for (i = 0; i < entry->h_count; i++) {
		if (entry->h[i] == h)
			return TRUE;
	}
	struct elf_link_hash_entry **new_h = bfd_realloc(entry->h, 
			(entry->h_count+1)*sizeof(struct elf_link_hash_entry *));
	if (!new_h)
		return FALSE;
	entry->h = new_h;
	entry->h[entry->h_count] = h;
	entry->h_count++;
	return TRUE;
}

/**
 * Add a reloc and symbol hash entry to a stub_entry.
 */
static bfd_boolean stub_add_reloc(stub_entry *entry, Elf_Internal_Rela *rel,
		struct elf_link_hash_entry *h)
{
    Elf_Internal_Rela **new_relocs = 
        bfd_realloc(entry->relocs,
                (entry->reloc_count+1) * sizeof(Elf_Internal_Rela *));
    if (!new_relocs)
        return FALSE;
    entry->relocs = new_relocs;
	int *new_index = bfd_realloc(entry->h_index, (entry->reloc_count+1)*sizeof(Elf_Internal_Rela *));
	if (!new_index)
		return FALSE;
	entry->h_index = new_index;
	if (!stub_add_hash_entry(entry, h))
		return FALSE;
	int i = stub_find_hash_entry(entry, h);
	if (i < 0)
		return FALSE;
    entry->relocs[entry->reloc_count] = rel;
	entry->h_index[entry->reloc_count] = i;
    entry->reloc_count++;
    return TRUE;
}

/**
 * Check relocation in a section to see if all the pc_relative relocations fit.
 * If they don't, generate stubs and redirect the branches to the stubs. The
 * stubs should then perform a direct jump to the target using the link
 * register.
 * Return TRUE on success and FALSE on failure.
 */
bfd_boolean
check_relocs(bfd *abfd, bfd *stub_bfd, bfd *out_bfd ATTRIBUTE_UNUSED, sec_ptr input_section,
		struct bfd_link_info *link_info, sec_ptr (*create_stub_section) (bfd *, bfd *, sec_ptr))
{
	Elf_Internal_Rela *relocs;
	Elf_Internal_Rela *rel, *relend;
	int changed = 0;
	relocs = _bfd_elf_link_read_relocs (abfd, input_section, NULL,
			NULL, link_info->keep_memory);
	Elf_Internal_Shdr *symtab_hdr;
	symtab_hdr = &elf_tdata (abfd)->symtab_hdr;
	rel = relocs;
	relend = relocs + input_section->reloc_count;
	for (; rel < relend; rel++)
	{
		unsigned long r_symndx;
		r_symndx = ELF32_R_SYM (rel->r_info);
		reloc_howto_type *howto;
		int r_type = ELF32_R_TYPE (rel->r_info);
		howto = rvex_elf_howto_table + (r_type);
		if (!howto->pc_relative)
			continue;
		if (r_symndx < symtab_hdr->sh_info)
			continue;
		bfd_vma relocation;
		struct elf_link_hash_entry *h;
		bfd_boolean r = global_reloc_wrapper(abfd, input_section, link_info, rel, &h, &relocation);
		if (!r)
			continue;
		relocation -= (input_section->output_section->vma
				+ input_section->output_offset);
		if ((signed long)relocation > 0)
			relocation += 4*1024*sizeof(stub_data);
		else if ((signed long)relocation < 0)
			relocation -= 4*1024*sizeof(stub_data);
		bfd_reloc_status_type res = bfd_check_overflow (howto->complain_on_overflow,
				howto->bitsize,
				howto->rightshift,
				bfd_arch_bits_per_address (abfd),
				relocation);
		if (res != bfd_reloc_overflow)
			continue;
		stub_entry *entry = get_stub_entry(input_section);
		if (!entry)
			return FALSE;
		if (stub_check_reloc(entry, rel))
			return FALSE;
		if (!stub_add_reloc(entry, rel, h))
			return FALSE;
		if (!entry->stub_section)
			entry->stub_section = create_stub_section(stub_bfd, abfd, input_section);
		long int stub_size = sizeof(stub_data);
		bfd_set_section_size(stub_bfd, entry->stub_section, entry->h_count * stub_size);
		changed = 1;
	}
	if (changed)
		return TRUE;
	return FALSE;
}

bfd_boolean rvex_finallize_relocs(bfd *stub_bfd, struct bfd_link_info *link_info)
{
    stub_entry *entry;
    for (entry = stub_list; entry != NULL; entry = entry->next) {
        sec_ptr stub_section = entry->stub_section;
        stub_section->contents = bfd_alloc(stub_bfd,
                bfd_get_section_size(stub_section));
        stub_section->reloc_count = 2*entry->h_count;
        int stub_size = sizeof(stub_data);
        long int i;
        for (i = 0; i < entry->h_count; i++) {
            unsigned int j;
            for (j = 0; j < stub_size/sizeof(unsigned int); j++) {
                bfd_put_32(stub_bfd, stub_data[j], stub_section->contents+
                        (i*stub_size + j*sizeof(unsigned int)));
            }
        }
        struct elf_link_hash_entry *sym = _bfd_elf_define_linkage_sym (
                stub_bfd, link_info, stub_section, stub_section->name);
        sym->other = ELF_ST_VISIBILITY(STV_DEFAULT);
        sym->type = STT_FUNC;
        bfd_elf_link_record_dynamic_symbol (link_info,
                sym);
        unsigned long int new_symindx;
        if (!sym_hashes_add_hash(entry->input_section->owner, sym, &new_symindx)) {
            printf("Could not add new symbol to old bfd\n");
            return FALSE;
        }
        for (i = 0; i < entry->reloc_count; i++) {
            Elf_Internal_Rela *rel = entry->relocs[i];
            rel->r_info = ELF32_R_INFO(new_symindx, ELF32_R_TYPE(rel->r_info));
            rel->r_addend += entry->h_index[i]*stub_size;
		}
		sym_hashes_add_hash(stub_bfd, NULL, NULL);
		for (i = 0; i < entry->h_count; i++) {
            struct elf_link_hash_entry *h = entry->h[i];
            unsigned long int new_symindx2;
            if (!sym_hashes_add_hash(stub_bfd, h, &new_symindx2)) {
                printf("Could not add new symbol to stub_bfd hashes.\n");
                return FALSE;
            }
            Elf_Internal_Rela *r2 = get_relocs(stub_section, 2);
            r2[0].r_offset = _bfd_elf_section_offset (stub_bfd, link_info,
                    stub_section,
                    i*stub_size + 4*sizeof(unsigned int));
            r2[0].r_info = ELF32_R_INFO (new_symindx2, R_RVEX_MOV_L);
            r2[0].r_addend = 0;
            r2[1].r_offset = _bfd_elf_section_offset (stub_bfd, link_info,
                    stub_section,
					i*stub_size + 5*sizeof(unsigned int));
            r2[1].r_info = ELF32_R_INFO (new_symindx2, R_RVEX_MOV_H);
            r2[1].r_addend = 0;
        }
    }
    return TRUE;
}

/**
 * Relocate a RVEX ELF section.  
 */
static bfd_boolean
elf32_rvex_relocate_section (bfd *output_bfd ATTRIBUTE_UNUSED, struct bfd_link_info *info,
                             bfd *input_bfd, asection *input_section,
                             bfd_byte *contents, Elf_Internal_Rela *relocs,
                             Elf_Internal_Sym *local_syms,
                             asection **local_sections)
{
	Elf_Internal_Shdr *symtab_hdr;
	struct elf_link_hash_entry **sym_hashes;
	Elf_Internal_Rela *rel, *relend;

	symtab_hdr = &elf_tdata (input_bfd)->symtab_hdr;
	sym_hashes = elf_sym_hashes (input_bfd);

	relend = relocs + input_section->reloc_count;
	for (rel = relocs; rel < relend; rel++)
	{
		int r_type;
		reloc_howto_type *howto;
		unsigned long r_symndx;
		Elf_Internal_Sym *sym;
		asection *sec;
		struct elf_link_hash_entry *h;
		bfd_vma relocation;
		bfd_reloc_status_type r;

		r_symndx = ELF32_R_SYM (rel->r_info);
		r_type = ELF32_R_TYPE (rel->r_info);
		howto = rvex_elf_howto_table + (r_type);

		h = NULL;
		sym = NULL;
		sec = NULL;
		if (r_symndx < symtab_hdr->sh_info)
		{
			sym = local_syms + r_symndx;
			sec = local_sections[r_symndx];
			rel->r_addend += sec->output_offset + sym->st_value;
			relocation = sec->output_section->vma;
		}
		else
		{
			bfd_boolean unresolved_reloc, warned, ignored;

			RELOC_FOR_GLOBAL_SYMBOL (info, input_bfd, input_section, rel,
					r_symndx, symtab_hdr, sym_hashes,
					h, sec, relocation,
					unresolved_reloc, warned, ignored);
			if (bfd_link_relocatable(info))
				continue;			//H global symbols are all resolved at final link time
		}

		if (sec != NULL && discarded_section (sec))
			RELOC_AGAINST_DISCARDED_SECTION (info, input_bfd, input_section,
					rel, 1, relend, howto, 0, contents);

		if (bfd_link_relocatable(info))
			continue;

		r = _bfd_final_link_relocate (howto, input_bfd, input_section,
				contents, rel->r_offset, relocation, rel->r_addend);

		if (r != bfd_reloc_ok)
		{
			const char *name;
			const char *msg = NULL;

			if (h != NULL)
				name = h->root.root.string;
			else
			{
				name = (bfd_elf_string_from_elf_section
						(input_bfd, symtab_hdr->sh_link, sym->st_name));
				if (name == NULL || *name == '\0')
					name = bfd_section_name (input_bfd, sec);
			}

			switch (r)
			{
				case bfd_reloc_overflow:
					(*info->callbacks->reloc_overflow)
								(info, (h ? &h->root : NULL), name, howto->name,
								 (bfd_vma) 0, input_bfd, input_section,
								 rel->r_offset);
					break;

				case bfd_reloc_undefined:
					(*info->callbacks->undefined_symbol)
								(info, name, input_bfd, input_section,
								 rel->r_offset, TRUE);
					break;

				case bfd_reloc_outofrange:
					msg = _("internal error: out of range error");
					goto common_error;

				case bfd_reloc_notsupported:
					msg = _("internal error: unsupported relocation error");
					goto common_error;

				case bfd_reloc_dangerous:
					msg = _("internal error: dangerous error");
					goto common_error;

				default:
					msg = _("internal error: unknown error");
					/* Fall through.  */

common_error:
					(*info->callbacks->warning)
								(info, msg, name, input_bfd, input_section,
								 rel->r_offset);
					break;
			}
		}
	}

	return TRUE;
}


/* Definitions for setting RVEX target vector.  */
#define TARGET_BIG_SYM                 rvex_elf32_vec
#define TARGET_BIG_NAME                "elf32-rvex"
#define ELF_ARCH                          bfd_arch_rvex
#define ELF_MACHINE_CODE                  EM_RVEX
#define ELF_MAXPAGESIZE                   0x1000
#define elf_symbol_leading_char           '_'

#define bfd_elf32_bfd_reloc_type_lookup   elf_rvex_reloc_type_lookup
#define bfd_elf32_bfd_reloc_name_lookup   elf_rvex_reloc_name_lookup
#define elf_info_to_howto                 elf_rvex_info_to_howto
#define elf_info_to_howto_rel             0
#define elf_backend_can_gc_sections       0
#define elf_backend_rela_normal           0
#define elf_backend_relocate_section      elf32_rvex_relocate_section

#include "elf32-target.h"

