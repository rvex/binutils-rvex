#include "as.h"
#include "safe-ctype.h"
#include "dwarf2dbg.h"
#include "opcode/rvex.h"
#include "elf/rvex.h"
#include "bfd.h"
#include "struc-symbol.h"

/* Opcode mnemonics hash table.  */
static struct hash_control *rvex_inst_hash;

static ins nop_instruction;
static int debug = 0;

/*variables related to instruction sorting*/
static node_list *nodes = NULL;
static graph_node **sorted_nodes = NULL;
static int *filled = NULL;

/*variables related to machine configuration*/
/*indicate whether to output a universal binary or not*/
static int generic_binary = <|generic_binary|>;
/*The issue width of the output is either 2, 4, or 8*/
static unsigned int issue_width = <|num_lanes|>;
/*String containing the location of the base functional units in hex:
 * _______|0123|4567|89AB|CDEF|
 *    alu | x x| x x| x x| x x|
 *    mul |  xx|  xx|  xx|  xx|
 *    mem |    |xxxx|    |xxxx|
 *     br |    |    |xxxx|xxxx|
 */
static const char *config_str = NULL;
/*The string containing the location of the FPUs in base-32.
 * _______|0123|4567|89AB|CDEF|GHIJ|KLMN|OPQR|STUV|
 *   falu | x x| x x| x x| x x| x x| x x| x x| x x|
 *   fmul |  xx|  xx|  xx|  xx|  xx|  xx|  xx|  xx|
 *   fcmp |    |xxxx|    |xxxx|    |xxxx|    |xxxx|
 * convif |    |    |xxxx|xxxx|    |    |xxxx|xxxx|
 * convfi |    |    |    |    |xxxx|xxxx|xxxx|xxxx|
 */
static const char *fconfig_str = NULL;
/* The string indicates how different lanes can borrow from each other.
 * the values where 1 lane can borrow should be comma seperated.
 * The strings for each different lane should be seperated by '.'.
 * Example: 1,2,3.0,2,3.0,1,3.0,1,2.*/
static const char *borrow_str = NULL;
/* This variable contains the actual configuration as parsed from the string passed by the user*/
static functional_unit_type *fu_configuration = NULL;
/* This variable contains the actual borrow configuration as parsed from the string.*/
static int **slot_borrow = NULL;
/*nopad causes bundles to be padded only to the next multiple of 2 when set.*/
static int padding = <|bundle_align|>;
/* Whether the assembler should automatically try to split a bundle if it cannot be scheduled */
static int autosplit = <|autosplit|>;

/* Other global variables.*/

static int start_function = 0;

/* Characters which always start a comment.  */
const char comment_chars[] = "#";

/* Characters which start a comment at the beginning of a line.  */
const char line_comment_chars[] = "#";

/* This array holds machine specific line separator characters.  */
const char line_separator_chars[] = "";

/* Chars that can be used to separate mant from exp in floating point nums.  */
const char EXP_CHARS[] = "eE";

/* Chars that mean this number is a floating point constant as in 0f12.456  */
const char FLT_CHARS[] = "f'";

const char rvex_symbol_chars[] = "-(";

/* Target-specific multicharacter options, not const-declared at usage.  */
const char *md_shortopts = "u";

enum arg_option {
	option_issue = OPTION_MD_BASE,
	option_config,
	option_fconfig,
	option_borrow,
	option_debug,
	option_nopad,
	option_pad,
	option_core,
	option_EB,
	option_relocs,
	option_autosplit,
	option_no_autosplit,
	option_generic,
	option_no_generic,
};

struct option md_longopts[] =
{
  {"issue", required_argument, NULL, option_issue},
  {"config", required_argument, NULL, option_config},
  {"fconfig", required_argument, NULL, option_fconfig},
  {"borrow", required_argument, NULL, option_borrow},
  {"debug", no_argument, NULL, option_debug},
  {"no-pad", no_argument, NULL, option_nopad},
  {"padding", required_argument, NULL, option_pad},
  {"mcore", required_argument, NULL, option_core},
  {"EB", no_argument, NULL, option_EB},
  {"emit-all-relocs", no_argument, NULL, option_relocs},
  {"autosplit", no_argument, NULL, option_autosplit},
  {"no-autosplit", no_argument, NULL, option_no_autosplit},
  {"generic", no_argument, NULL, option_generic},
  {"no-generic", no_argument, NULL, option_no_generic},
  {NULL, no_argument, NULL, 0}
};
size_t md_longopts_size = sizeof (md_longopts);

static void output_instruction(ins *insn, int last);
static void build_syllable_follow(ins *insn, ins *insn_src);
static void get_correct_instruction(ins *insn);
static void init_insn(ins *insn);


/* Process machine-dependent command line options.  Called once for
   each option on the command line that the machine-independent part of
   GAS does not understand.  */
int
md_parse_option (int c, const char *arg)
{
	switch (c)
	{
		case option_generic:
		case 'u':
			generic_binary = 1;
			break;
		case option_no_generic:
			generic_binary = 0;
			break;
		case option_issue:
			issue_width = atoi(arg);
			if (issue_width != 2 &&
					issue_width != 4 &&
					issue_width != 8)
				issue_width = 4;
			break;
		case option_config:
			if (config_str)
				free((void *)config_str);
			config_str = strdup(arg);
			break;
		case option_fconfig:
			if (fconfig_str)
				free((void *)fconfig_str);
			fconfig_str = strdup(arg);
			break;
		case option_borrow:
			if (borrow_str)
				free((void *)borrow_str); 
			borrow_str = strdup(arg);
			break;
		case option_debug:
			debug = 1;
			break;
		case option_nopad:
			padding = 2;
			break;
		case option_pad:
			padding = atoi(arg);
			break;
		case option_core: //ignore
			break;
		case option_EB: //ignore
			break;
		case option_relocs: //ignore
			break;
		case option_autosplit:
			autosplit = 1;
			break;
		case option_no_autosplit:
			autosplit = 0;
			break;
		default:
			return 0;
			break;
	}
	return 1;
}

/* Machine-dependent usage-output.  */
void
md_show_usage (FILE *stream)
{
	fprintf(stream, "r-VEX options:\n\
  -u,--generic            output generic binary\n\
  --no-generic            disable generic binaries\n\
  --issue WIDTH           the issue width: 2, 4 or 8\n\
  --config LAYOUT         position of the integer functional units\n\
  --fconfig FLAYOUT       position of the floating point functional units\n\
  --borrow BORROW         configuration of borrowing for long immediates\n\
  --no-pad                only pad bundles to multiples of 2 instructions\n\
  --padding COUNT         only pad bundles to multiples of COUNT instructions\n\
  --autosplit             automatically split bundles when they cannot be\n\
                            scheduled individually\n\
  --no-autosplit          disable autosplit\n\
\n\
default: <|generic_binary_flag|>--issue <|num_lanes|> --config <|config_str|> --fconfig <|fconfig_str|>\n\
  --borrow <|borrow_str|> --padding <|bundle_align|><|autosplit_flag|>\n\
\n\
LAYOUT: hex string with one digit per lane (lowest index first):\n\
         .----.----.----.----.\n\
         |0123|4567|89AB|CDEF|\n\
.--------+----+----+----+----|\n\
|    alu | x x| x x| x x| x x|\n\
|    mul |  xx|  xx|  xx|  xx|\n\
|    mem |    |xxxx|    |xxxx|\n\
|     br |    |    |xxxx|xxxx|\n\
'--------'----'----'----'----'\n\
\n\
FLAYOUT: base-32 string with one digit per lane (lowest index first):\n\
         .----.----.----.----.----.----.----.----.\n\
         |0123|4567|89AB|CDEF|GHIJ|KLMN|OPQR|STUV|\n\
.--------+----+----+----+----+----+----+----+----|\n\
|   fadd | x x| x x| x x| x x| x x| x x| x x| x x|\n\
|   fmul |  xx|  xx|  xx|  xx|  xx|  xx|  xx|  xx|\n\
|   fcmp |    |xxxx|    |xxxx|    |xxxx|    |xxxx|\n\
| convif |    |    |xxxx|xxxx|    |    |xxxx|xxxx|\n\
| convfi |    |    |    |    |xxxx|xxxx|xxxx|xxxx|\n\
'--------'----'----'----'----'----'----'----'----'\n\
\n\
BORROW: point-separated list of comma-separated lane indices for each lane,\n\
indicating which lanes a lane can borrow from. Example:\n\
  1.0.0,3.1,2\n\
Lane 0 can borrow from 1 (neighbor),\n\
Lane 1 can borrow from 0 (neighbor),\n\
Lane 2 can borrow from 3 (neighbor) and 0 (previous),\n\
Lane 3 can borrow from 2 (neighbor) and 0 (previous)\n\
");
	return;
}


/*dont know what this does*/
const char *
md_atof (int type, char *litP, int *sizeP)
{
	return ieee_md_atof (type, litP, sizeP, target_big_endian);
}

static void func_start(int a ATTRIBUTE_UNUSED)
{
	s_func(0);
	start_function = 1;
}

static void handle_data(int a)
{
	char *saved_pointer = input_line_pointer;
	unsigned int times = 1;
	unsigned int i;
	expressionS exp;
	while (*input_line_pointer != '\0' && *input_line_pointer != '\n')
	{
		input_line_pointer++;
		if(*input_line_pointer == ':')
		{
			char *temp = input_line_pointer;
			input_line_pointer++;
			expression(&exp);
			times = exp.X_add_number;
			demand_empty_rest_of_line();
			while (temp < input_line_pointer - 1)
			{
				*temp = '\0';
				temp++;
			}
			break;
		}
	}
	for (i = 0; i < times; i++)
	{
		input_line_pointer = saved_pointer;
		cons(a);
	}
}

void rvex_cons_align (int nbytes)
{
	int nalign;

	nalign = 0;
	while ((nbytes & 1) == 0)
	{
		++nalign;
		nbytes >>= 1;
	}

	if (nalign == 0)
		return;

	if (now_seg == absolute_section)
	{
		if ((abs_section_offset & ((1 << nalign) - 1)) != 0)
			as_warn (_("misaligned data"));
		return;
	}

	frag_var (rs_align_test, 1, 1, (relax_substateT) 0,
			(symbolS *) NULL, (offsetT) nalign, (char *) NULL);

	record_alignment (now_seg, nalign);
}

const pseudo_typeS md_pseudo_table[] =
{
	{"proc", func_start, 0 },
	{"align", s_align_bytes, 0 },
	{"endp", s_func, 1 },
	{"real8", handle_data, 8},
	{"real4", handle_data, 4},
	{"data8", handle_data, 8},
	{"data4", handle_data, 4},
	{"data2", handle_data, 2},
	{"data1", handle_data, 1},
	{"word", handle_data, 4},
	{"hword", handle_data, 2},
	{"byte", handle_data, 1},
	{"comment", s_ignore, 0},
	{"sversion", s_ignore, 0},
	{"rta", s_ignore, 0},
	{"trace", s_ignore, 0},
	{"entry", s_ignore, 0},
	{"return", s_ignore, 0},
	{"import", s_ignore, 0},
	{"call", s_ignore, 0},
	{"assume", s_ignore, 0},
	{"nonopinsertion", s_ignore, 0},
	{"nopinsertion", s_ignore, 0},
	{"type", s_ignore, 0},
	{0, 0, 0}
};


static void build_nop(ins *insn);

/**
	return the functional unit type that the instruction requires
*/
static unsigned int get_inst_fu_type(ins *insn)
{
	const inst *instruction = insn->instruction;
	switch(instruction->type)
	{
		case mult:
			return MUL;
		case arith:
		case addcg:
			return ALU;
		case mem:
			return MEM;
		case branch:
			return BR;
		case fadd:
			return FADD;
		case fmul:
			return FMUL;
		case fcmp:
			return FCMP;
		case convif:
			return CONVIF;
		case convfi:
			return CONVFI;
		case follow:
		case special:
		case unknown_inst:
		default:
			return NONE;
	}
}

static node_list *createList(void)
{
	node_list *list = xmalloc(sizeof(node_list));
	list->list = NULL;
	list->size = 0;
	list->count = 0;
	return list;
}

static void clearList(node_list *list)
{
	if(list->list)
		xfree(list->list);
	list->list = NULL;
	list->size = 0;
	list->count = 0;
}

static void destroyList(node_list *list)
{
	if (!list)
		return;
	if (list->list)
		xfree(list->list);
	xfree(list);
}

static void listAddNode(node_list *list, graph_node *node)
{
	graph_node **temp_list = NULL;
	if(list->count >= list->size)
	{
		list->size += issue_width;
		temp_list = realloc(list->list, list->size * sizeof(graph_node *));
		if (!temp_list)
		{
			list->size -= issue_width;
			as_warn("allocation failed");
			return;
		}
		list->list = temp_list;
	}
	list->list[list->count] = node;
	list->count++;
	return;
}

static void listRemoveNode(node_list *list, graph_node *node)
{
	unsigned int i;
	for (i = 0; i < list->count; i++)
	{
		if (list->list[i] == node)
			break;
	}
	for (i += 1; i < list->count; i++)
	{
		list->list[i-1] = list->list[i];
	}
	list->count -= 1 ;
}

/* Create a node for the instruction "id" and initialize the other fields to 0.
 */
static graph_node *build_node(int id)
{
	graph_node *node = xmalloc(sizeof(graph_node));
	node->follow = 0;
	node->end = (issue_width - 1);
	node->start = 0;
	node->id = id;
	node->instruction = xmalloc(sizeof(ins));
	init_insn(node->instruction);
	node->follow_node = NULL;
	node->scheduled = 0;
	node->parent = createList();
	node->child = createList();
	return node;
}

/* Create the follow node from the base node
 * and add it to the list of nodes
 */
static void createFollowNode(graph_node *node)
{
	graph_node *follow_node = build_node(nodes->count);
	follow_node->follow = 1;
	node->follow_node = follow_node;
	build_syllable_follow(follow_node->instruction, node->instruction);
	get_correct_instruction(follow_node->instruction);
	listAddNode(nodes, follow_node);
}

/* Check if arg1 and arg2 represent the same register.
 * We check using oper because a memory reference is also
 * a register access. oper will still show as a register.
 */
static int matchRegisterArgument(argument *arg1, argument *arg2)
{
	if (arg1->oper->op_type == arg2->oper->op_type)
	{
		if (arg1->reg_number == arg2->reg_number &&
				arg1->reg_cluster == arg2->reg_cluster)
			return 1;
	}
	return 0;
}

/* Check if any other instruction writes to the argument arg.
 * If so add that node as a child to this one.
 */
static void checkArgument(graph_node *node, argument *arg)
{
	int j;
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		if (node == nodes->list[i])
			continue;
		for (j = 0; j < nodes->list[i]->instruction->nargs; j++)
		{
			if (nodes->list[i]->instruction->arguments[j].oper &&
					nodes->list[i]->instruction->arguments[j].oper->access == write_access)
			{
				if (matchRegisterArgument(arg, &nodes->list[i]->instruction->arguments[j]))
				{
					listAddNode(nodes->list[i]->child, node);
					listAddNode(node->parent, nodes->list[i]);
				}
			}
		}
	}
}

static void checkBranch(graph_node *node)
{
	unsigned int i = 0;
	for (i = 0; i < nodes->count; i++)
	{
		if (node == nodes->list[i])
			continue;
		if (get_inst_fu_type(nodes->list[i]->instruction) == BR)
		{
			listAddNode(nodes->list[i]->child, node);
			listAddNode(node->parent, nodes->list[i]);
		}
	}
}

/* For each operand of the instruction that is read, 
 * check if another instruction writes to it, and if so,
 * add it as a child to this node.
 */
static void checkChildren(graph_node *node)
{
	int i;
	for (i = 0; i < node->instruction->nargs; i++)
	{
		argument *arg = &node->instruction->arguments[i];
		if(arg->oper == NULL)
			continue;
		if (arg->oper->access == read_access &&
				!(arg->type == reg_g && arg->reg_number == 0))
		{
			checkArgument(node, arg);
		}
	}
}

static void build_branch_graph(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		checkBranch(nodes->list[i]);
	}
}

/* Detect all the children for each node.
 */
static void build_graph(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		checkChildren(nodes->list[i]);
	}
	build_branch_graph();
}

static void destroyNode(graph_node *node)
{
	xfree(node->instruction);
	destroyList(node->child);
	destroyList(node->parent);
	xfree(node);
	return;
}

static void clear_graph(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		clearList(nodes->list[i]->parent);
		clearList(nodes->list[i]->child);
	}
	for (i = 0; i < issue_width; i++)
	{
		filled[i] = 0;
		sorted_nodes[i] = NULL;
	}
}

static void destroy_graph(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		destroyNode(nodes->list[i]);
		nodes->list[i] = NULL;
	}
	nodes->count = 0;
	for (i = 0; i < issue_width; i++)
	{
		filled[i] = 0;
		sorted_nodes[i] = NULL;
	}
}

static void print_node(graph_node *node)
{
	unsigned int i;
	printf("[%d]", node->id);
	if (node->follow_node)
	{
		printf(":[%d]", node->follow_node->id);
	}
	if (node->child->count > 0)
		printf("->");
	for (i = 0; i < node->child->count; i++)
	{
		if (i > 0)
			printf(",");
		printf("[%d]", node->child->list[i]->id);
	}
	printf("\n");
}

static void print_graph(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		print_node(nodes->list[i]);
	}
}


static int even(int val)
{
	if (val % 2)
		return 0;
	else
		return 1;
}

static void updateStartEnd(graph_node *node, int issue)
{
	unsigned int i;
	int new_end = even(issue)? issue+1 : issue;
	int new_start = even(issue)? issue : issue-1;
	if (get_inst_fu_type(node->instruction) == BR)
	{
		new_end = issue;
	}

	for (i = 0; i < node->child->count; i++)
	{
		if (node->child->list[i]->end > new_end)
			node->child->list[i]->end = new_end;
	}
	for (i = 0; i < node->parent->count; i++)
	{
		if (node->parent->list[i]->start < new_start)
			node->parent->list[i]->start = new_start;
	}
}

/**
 * Restore start and end indexes of the children of node to what they were
 * before node was scheduled.
 */
static void restoreStartEnd(graph_node *node)
{
	unsigned int i;
	for (i = 0; i < node->child->count; i++)
		node->child->list[i]->end = issue_width-1;
	for (i = 0; i < node->parent->count; i++)
		node->parent->list[i]->start = 0;
	for (i = 0; i < (unsigned)issue_width; i++)
		if (sorted_nodes[i])
			updateStartEnd(sorted_nodes[i], i);
}

static void issueNode(graph_node *node, int issue)
{
	sorted_nodes[issue] = node;
	filled[issue] = 1;
	node->scheduled = 1;
	updateStartEnd(node, issue);
	if (IS_LONG_IMM_INSTR(node->instruction))
	{
		node->follow_node->instruction->arguments[0].imm_value = issue;
	}
	return;
}

static void unIssueNode(graph_node *node, int issue)
{
	sorted_nodes[issue] = NULL;
	filled[issue] = 0;
	node->scheduled = 0;
	restoreStartEnd(node);
	if (IS_LONG_IMM_INSTR(node->instruction))
	{
		node->follow_node->instruction->arguments[0].imm_value = 0;
	}
	return;
}

static int isEmpty(int issue)
{
	return !filled[issue];
}

static int matchFunctionType(graph_node *node, int issue)
{
	return get_inst_fu_type(node->instruction) & fu_configuration[issue];
}

static int hasFollow(graph_node *node)
{
	if (node->follow_node)
		return 1;
	return 0;
}

static int hasEmptyBorrow(int issue)
{
	unsigned int i;
	for (i = 0; i < issue_width - 1; i++)
	{
		if (slot_borrow[issue][i] == -1)
			return 0;
		if (isEmpty(slot_borrow[issue][i]))
			return 1;
	}
	return 0;
}

static int canIssueNode(graph_node *node, int issue)
{
	if (matchFunctionType(node, issue) && isEmpty(issue))
	{
		if (padding && generic_binary && get_inst_fu_type(node->instruction) == BR)
		{
			if (issue % padding != padding-1)
				return 0;
			else
				return 1;
		}

		if (hasFollow(node) && !hasEmptyBorrow(issue))
			return 0;
		else
			return 1;
	}
	return 0;
}

static int issueCount(graph_node *node)
{
	int i;
	int count = 0;
	for (i = node->start; i < node->end; ++i)
	{
		if (canIssueNode(node, i))
			++count;
	}
	return count;
}

/* Find the next node to schedule.
 * This returns the node with the most children that has not yet been scheduled.
 * If multiple nodes have an equal number of children, return the one with the
 * fewest placement options.
 */
static graph_node *getReadyNode(void)
{
	unsigned int i;
	unsigned int max_val = 0;
	graph_node *max_node = NULL;
	for (i = 0; i < nodes->count; i++)
	{
		if(nodes->list[i]->follow)
			continue;
		if (nodes->list[i]->scheduled)
			continue;
		if (nodes->list[i]->child->count > max_val)
		{
			max_val = nodes->list[i]->child->count;
			max_node = nodes->list[i];
		}
		if (nodes->list[i]->child->count == max_val)
		{
			if (!max_node || issueCount(nodes->list[i]) < issueCount(max_node))
			{
				max_val = nodes->list[i]->child->count;
				max_node = nodes->list[i];
			}
		}

	}
	return max_node;
}

static void printSortedNodes(void)
{
	unsigned int i;
	for (i = 0; i < issue_width; i++)
	{
		if (sorted_nodes[i] != NULL)
		{
			printf("[%d] ", sorted_nodes[i]->id);
		}
		else
		{
			printf("[_] ");
		}
	}
	printf("\n");
}

static int scheduleDepthFirst(int);

static int scheduleFollowNode(graph_node *node, int issue)
{
	unsigned int i;
	if (node == NULL)
		return 1;
	for (i = 0; i < issue_width - 1; i++)
	{
		if (slot_borrow[issue][i] == -1)
			return 0;
		if (isEmpty(slot_borrow[issue][i]))
		{
			issueNode(node, slot_borrow[issue][i]);
			if (scheduleDepthFirst(0))
				return 1;
			if (debug)
				printSortedNodes();
			unIssueNode(node, slot_borrow[issue][i]);
		}
	}
	return 0;
}

static int isGraphScheduled(void)
{
	unsigned int i;
	if (nodes->count == 0)
		return 0;
	for (i = 0; i < nodes->count; i++)
		if (!nodes->list[i]->scheduled)
			return 0;
	return 1;
}

/**
 * Count the number of NOPs in an instruction bundle before the last non-NOP
 * instruction. This is used to determine how well an instruction bundle is
 * packed.
 */
static int countNops(graph_node **list)
{
	int i;
	int count = 0;
	for (i = issue_width-1; i >= 0; --i)
	{
		if (list[i] != NULL)
			break;
	}
	for (; i >= 0; --i)
	{
		if (list[i] == NULL)
			++count;
	}
	return count;
}

static int findLastOperation(void);

/**
 * Checks whether the current schedule is valid. Does not check for
 * everything - this assumes nodes are only scheduled when canIssueNode()
 * returns true. Currently, this checks whether any branch instruction appears
 * in the last slot with a branch unit, respecting the position of the stop
 * bit (this cannot be determined on-the-fly because the position of the stop
 * bit is not known until all nodes have been scheduled).
 */
static int checkSchedule(void) {
	int i;
	int syl_count;
	int br_slot;
	
	// Determine bundle size.
	if (padding)
	{
		syl_count = findLastOperation() + 1;
		syl_count = ((syl_count + padding - 1) / padding) * padding;
	}
	else
	{
		syl_count = issue_width;
	}
	
	// Determine the issue slot which has the active branch unit for this
	// operation. This will return -1 if no slot is available.
	for (br_slot = syl_count-1; br_slot >= 0; br_slot--)
		if (fu_configuration[br_slot] & BR)
			break;
	
	// Fail if there is a branch instruction in any other slot than br_slot.
	for (i = 0; i < syl_count; i++)
		if ((sorted_nodes[i]) && (get_inst_fu_type(sorted_nodes[i]->instruction) & BR) && (i != br_slot))
			return 0;
	
	return 1;
}

/**
 * Packs instructions into an instruction bundle using a depth first search.
 * Search for an instruction packing with 0 nops between instructions. If none
 * is found return the one with the least nops between instructions. The packed
 * instructions are in the sorted_nodes array.
 * A return value of 0 means no packing was found, while 1 means one was found.
 * The start parameter indicates whether the functions is being called
 * recursively or not. 1 means it is the first call (non-recursive). 0 means it
 * is a recursive call. Basically start should always be 1, except when calling
 * from scheduleDepthFirst or scheduleFollowNode.
 */
static int scheduleDepthFirst(int start)
{
	int i;
	static graph_node **spare_nodes = NULL;
	graph_node *node = getReadyNode();
	if (node == NULL)
	{
		if (checkSchedule())
		{
			int first = 0;
			if (!spare_nodes)
			{
				first = 1;
				spare_nodes = calloc(issue_width, sizeof(graph_node *));
			}
			if (first || countNops(sorted_nodes) < countNops(spare_nodes))
				for (i = 0; i < (int) issue_width; ++i)
					spare_nodes[i] = sorted_nodes[i];
			if (countNops(sorted_nodes) == 0)
			{
				xfree(spare_nodes);
				spare_nodes = NULL;
				return 1;
			}
		}
		return 0;
	}
	for (i = node->start; i < (node->end+1); i++)
	{
		if (canIssueNode(node, i))
		{
			issueNode(node, i);
			if (node->follow_node)
			{
				if (scheduleFollowNode(node->follow_node, i))
					return 1;
			}
			else
			{
				if (scheduleDepthFirst(0))
					return 1;
			}
			if (debug)
				printSortedNodes();
			unIssueNode(node, i);
		}
	}
	if (start && spare_nodes)
	{
		for (i = 0; i < (int) issue_width; ++i)
			if (spare_nodes[i])
				issueNode(spare_nodes[i], i);
		xfree(spare_nodes);
		spare_nodes = NULL;
		if (checkSchedule())
			return 1;
	}
	return 0;
}

static int findLastOperation(void)
{
	int i;
	for (i = issue_width - 1; i >= 0; i--)
	{
		if (filled[i])
			return i;
	}
	return 0;
}

/* Output the scheduled instructions from the graph.
 */
static void outputInstructionGraph(void)
{
	int i;
	int last = findLastOperation();
	int loop_bound = issue_width;

	if (padding)
		loop_bound = last + 1 + (((padding) - ((last+1) % padding)) % padding);

	for (i = 0; i < loop_bound; i++)
	{
		if (filled[i])
			output_instruction(sorted_nodes[i]->instruction, i == (loop_bound-1));
		else
			output_instruction(&nop_instruction, i == (loop_bound-1));
	}
	if (debug)
		printSortedNodes();
}

static void split_return_and_stack_pop(graph_node *add_node)
{
	unsigned int i;
	node_list *tmp_nodes = createList();
	clear_graph();
	build_graph();

	for (i = 0; i < nodes->count; i++)
	{
		graph_node *node = nodes->list[i];
		if(node->follow || node->parent->count != 0 || node == add_node)
			continue;

		listRemoveNode(nodes, node);
		listAddNode(tmp_nodes, node);
		if (node->follow_node)
		{
			listRemoveNode(nodes, node->follow_node);
			listAddNode(tmp_nodes, node->follow_node);
		}
		clear_graph();
		build_graph();
		/**
		 * If scheduling succeeds exit the loop, otherwise restart the loop from the
		 * beginning since we have removed nodes from the graph.
		 */
		if(scheduleDepthFirst(1))
		{
			outputInstructionGraph();
			break;
		}
		else
		{
			i = -1;
		}
	}
	if (!isGraphScheduled())
	{
		as_bad("Failed to split return and stack pop.\n");
		return;
	}
	destroy_graph();
	destroyList(nodes);
	nodes = tmp_nodes;
	clear_graph();
}

static void combine_return_and_stack_pop(void)
{
	unsigned int i;
	graph_node *return_node = NULL;
	graph_node *add_node = NULL;
	for (i = 0; i < nodes->count; i++)
	{
		if ((strcmp(nodes->list[i]->instruction->instruction->mnemonic, "return") == 0 && nodes->list[i]->instruction->nargs == 1) ||
				(strcmp(nodes->list[i]->instruction->instruction->mnemonic, "rfi") == 0 && nodes->list[i]->instruction->nargs == 0))
		{
			return_node = nodes->list[i];
			break;
		}
	}
	if (!return_node)
		return;
	for (i = 0; i < nodes->count; i++)
	{
		if (strcmp(nodes->list[i]->instruction->instruction->mnemonic, "add") == 0)
		{
			if (nodes->list[i]->instruction->arguments[0].reg_number == 1)
			{
				add_node = nodes->list[i];
				break;
			}
		}
	}
	if (!add_node)
		return;
	if (return_node->instruction->instruction->nargs == 4)
	{
		as_bad("Invalid combination of return and stack pop");
		return;
	}
	if (add_node->instruction->arguments[1].reg_number != 1 ||
			add_node->instruction->arguments[2].type != imm)
	{
		//try to split the stack pop and return into seperate instructions
		split_return_and_stack_pop(add_node);
		return;
	}
	if (hasFollow(add_node))
	{
		if (!(add_node->instruction->exp.X_op == O_constant))
		{
			as_bad("Stack pop using label as value.");
			return;
		}
		listRemoveNode(nodes, add_node->follow_node);
		destroyNode(add_node->follow_node);
	}
	if ((add_node->instruction->arguments[2].imm_value >= (1<<(BRANCH_IMM_SIZE-1))) ||
		(add_node->instruction->arguments[2].imm_value < -(1<<(BRANCH_IMM_SIZE-1))))
	{
		as_bad("Stack pop out of immediate range.");
		return;
	}
	
	/*printf("\n\n*********************************************\n\n");
	printf("original return_node:\n");
	print_instruction(return_node->instruction);
	printf("add_node:\n");
	print_instruction(add_node->instruction);
	printf("imm_value = %d\n", add_node->instruction->arguments[2].imm_value);*/
	
	// Decrement insn index to get the complete (real) version of the insn.
	return_node->instruction->instruction--;
	
	// Move all arg data over by 3 slots; the first 3 slots are now the stack pointer
	// register as dest and src and the immediate to add to it.
	for (i = 0; i < (unsigned int)return_node->instruction->nargs; i++)
	{
		return_node->instruction->arguments[i+3] = return_node->instruction->arguments[i];
	}
	
	// Set the register numbers for the src/dest reg to 1 to select stack pointer.
	return_node->instruction->arguments[0].reg_number = 1;
	return_node->instruction->arguments[1].reg_number = 1;
	
	// Set the immediate.
	return_node->instruction->arguments[2].imm_value = add_node->instruction->arguments[2].imm_value;
	
	// Update the instruction information to the new format.
	return_node->instruction->nargs = return_node->instruction->instruction->nargs;
	for (i = 0; i < (unsigned int)return_node->instruction->nargs; i++)
	{
		return_node->instruction->arguments[i].oper = return_node->instruction->instruction->operands + i;
		return_node->instruction->arguments[i].type = return_node->instruction->instruction->operands[i].op_type;
	}
	
	//return_node->instruction->arguments[0].imm_value = add_node->instruction->arguments[2].imm_value;
	//return_node->instruction->arguments[0].oper = &return_node->instruction->instruction->operands[2];
	//return_node->instruction->arguments[0].type = imm;
	
	/*printf("new return_node:\n");
	print_instruction(return_node->instruction);
	printf("fields:\n");
	for (i = 0; i < (unsigned int)return_node->instruction->nargs; i++)
	{
		if (return_node->instruction->arguments[i].oper == NULL)//skip arguments that are hardwired such as in return
			continue;
		printf("%u: ", i);
		switch (return_node->instruction->arguments[i].type)
		{
			case reg_l:
				printf("link reg %d\n", return_node->instruction->arguments[i].reg_number);
				break;
			case reg_b:
				printf("branch reg %d", return_node->instruction->arguments[i].reg_number);
				break;
			case reg_g:
				printf("gpreg %d", return_node->instruction->arguments[i].reg_number);
				break;
			case imm:
				printf("immediate %d", return_node->instruction->arguments[i].imm_value);
				break;
			default:
				break;
		}
		printf(", size=%d, shift=%d\n", return_node->instruction->arguments[i].oper->size, return_node->instruction->arguments[i].oper->shift);
	}
	printf("\n*********************************************\n\n\n");*/
	
	listRemoveNode(nodes, add_node);
	destroyNode(add_node);
}



/*
 * The open64 compiler sometimes generates a select to the link register.
 * The rvex cannot handle this because the link register is not mirrored with general regster r63
 * as is the case with st200. Therefore we need to split it. We're using register 63 for this,
 * which open64 keeps available.
 */


static void split_slct_lr(graph_node *slct_node)
{
	unsigned int i;
	node_list *tmp_nodes = createList();
	graph_node *node;
	clear_graph();
	build_graph();

	//first, convert the select insn to target a free general-purpose reg. Open64 always leaves r63 free.
	slct_node->instruction->arguments[0].type = reg_g;
	slct_node->instruction->arguments[0].reg_number = 63;

	//Now, move all instructions except the slct (and possibly its accompanying long immediate) to a new bundle
	//Go through the nodes in reverse direction because we're removing (some) of them as we go
	for (i = nodes->count; i--;)
	{
		node = nodes->list[i];
		//printf("%d = %s\n", i, node->instruction->instruction->mnemonic);
		if(node->follow || node->parent->count != 0 || node == slct_node)
			continue;
		listRemoveNode(nodes, node);
		listAddNode(tmp_nodes, node);
		if (node->follow_node)
		{
			listRemoveNode(nodes, node->follow_node);
			listAddNode(tmp_nodes, node->follow_node);
		}
	}
	clear_graph();
	build_graph();

	//schedule and output the bundle (that should now only contain the slct instruction)
	if(scheduleDepthFirst(1))
	{
		outputInstructionGraph();
	}
	if (!isGraphScheduled())
	{
		as_bad("Failed to split slct to link register.\n");
		return;
	}
	destroy_graph();
	destroyList(nodes);

	//the temporary node list now contains the original operations without the slct. Put it back as the normal nodes list, add the extra move into the link register and continue as normal.
	nodes = tmp_nodes;
	node = build_node(nodes->count);
	node->instruction->instruction = (const inst *) hash_find (rvex_inst_hash, "movtl");
	node->instruction->nargs = 2;
	node->instruction->arguments[0].type = reg_l;
	node->instruction->arguments[0].reg_number = 0;
	node->instruction->arguments[1].type = reg_g;
	node->instruction->arguments[1].reg_number = 63;
	get_correct_instruction(node->instruction);
	//node->instruction->arguments[1].oper = &node->instruction->instruction->operands[1];
	listAddNode(nodes, node);
	clear_graph();
}

static void fix_select_to_lr(void)
{
	unsigned int i;
	for (i = 0; i < nodes->count; i++)
	{
		if (strncmp(nodes->list[i]->instruction->instruction->mnemonic, "slct", 4) == 0)
		{
			if (nodes->list[i]->instruction->arguments[0].type == reg_l)
			{
				split_slct_lr(nodes->list[i]);
				break;
			}
		}
	}
}

static void auto_split(void)
{
	unsigned int i;
	while (1)
	{
		node_list *tmp_nodes = createList();

		/**
		 * Remove nodes without parents one by one until the current graph can be
		 * scheduled. Skip follow nodes since they are dealth with as part of the
		 * node they belong to.
		 */
		for (i = 0; i < nodes->count; i++)
		{
			graph_node *node = nodes->list[i];
			if(node->follow || node->parent->count != 0)
				continue;

			listRemoveNode(nodes, node);
			listAddNode(tmp_nodes, node);
			if (node->follow_node)
			{
				listRemoveNode(nodes, node->follow_node);
				listAddNode(tmp_nodes, node->follow_node);
			}
			clear_graph();
			build_graph();
			/**
			 * If scheduling succeeds exit the loop, otherwise restart the loop from the
			 * beginning since we have removed nodes from the graph.
			 */
			if(scheduleDepthFirst(1))
			{
				outputInstructionGraph();
				break;
			}
			else
			{
				i = -1;
			}
		}
		if (!isGraphScheduled())
		{
			as_bad("Failed to split bundle\n");
			return;
		}
		destroy_graph();
		destroyList(nodes);
		nodes = tmp_nodes;
		clear_graph();
		build_graph();
		if(scheduleDepthFirst(1))
			return;
	}
}

/**
	Handle the double colon and semicolon
*/
int rvex_unrecognized_line (char c)
{
	switch (c)
	{
		case ':':
			if (input_line_pointer[-1] == ':')
			{
				//input_line_pointer++;
				if(line_label != NULL)
				{
					S_SET_EXTERNAL(line_label);
				}
				demand_empty_rest_of_line ();
				return 1;
			}
			break;
		case ';':
			/*this signals the end of an instruction, so emit it*/
			if (input_line_pointer[-1] == ';')
			{
				combine_return_and_stack_pop();
				fix_select_to_lr();
				if (generic_binary)
					build_graph();
				if (debug)
					print_graph();
				if (nodes->count > 0) //this means the bundle is not empty
				{
					if(!scheduleDepthFirst(1))
					{
						if (autosplit && generic_binary)
							auto_split();
						else
							as_bad("No valid schedule found.");
					}
				}
				outputInstructionGraph();
				destroy_graph();
				input_line_pointer++;
				demand_empty_rest_of_line();
				return 1;
			}
			break;
		default:
			break;
	}
	return 0;
}

/*hook for undefined symbols not needed for now*/
symbolS *
md_undefined_symbol (char *name ATTRIBUTE_UNUSED)
{
	return 0;
}

/**
	Set symbols in the data segment to objects
	Set symbols to functions if they are a function name.
*/
void rvex_check_label(symbolS *label)
{
	if (start_function != 1)
	{
		/*if the label is in the data section it is an object*/
		if (S_GET_SEGMENT(label) == data_section)
		{
			symbol_get_bfdsym (label)->flags |= BSF_OBJECT;
		}
		return;
	}
	start_function = 0;
	symbol_get_bfdsym (label)->flags |= BSF_FUNCTION;
}

void
md_convert_frag (bfd *abfd ATTRIBUTE_UNUSED, asection *sec ATTRIBUTE_UNUSED, fragS *fragP ATTRIBUTE_UNUSED)
{
	printf("md_convert_frag\n");
	return;
}

int
md_estimate_size_before_relax (fragS *fragp ATTRIBUTE_UNUSED, asection *seg ATTRIBUTE_UNUSED)
{
	printf("md_estimate_size_before_relax\n");
	return 4;
}

/**
	Create relocation table entry for labels (call, branch, goto, and memory
	operations). At the moment all of these generate relocation entry, however
	probably we only need these for memory operations, and calls to functions
	defined in a seperate file.
**/
arelent *
tc_gen_reloc (asection *section ATTRIBUTE_UNUSED, fixS * fixP)
{
	arelent * reloc;

	reloc = xmalloc (sizeof (arelent));
	reloc->sym_ptr_ptr  = xmalloc (sizeof (asymbol *));
	*reloc->sym_ptr_ptr = symbol_get_bfdsym (fixP->fx_addsy);
	reloc->address = fixP->fx_frag->fr_address + fixP->fx_where;
	reloc->addend = fixP->fx_offset;

	/* Jumps are relative to the next bundle, so get the address of the next
	 * bundle with md_pcrel_from and subtract it from the address of the reloc.*/
	if (fixP->fx_r_type == BFD_RELOC_RVEX_BRANCH)
		reloc->addend += reloc->address - md_pcrel_from (fixP);

	reloc->howto = bfd_reloc_type_lookup (stdoutput, fixP->fx_r_type);
	if (reloc->howto == NULL)
    {
		as_bad_where (fixP->fx_file, fixP->fx_line,
				_("internal error: reloc %d (`%s') not supported by object file format"),
				fixP->fx_r_type,
				bfd_get_reloc_code_name (fixP->fx_r_type));
		return NULL;
    }

	return reloc;
}

/**
 * Find the address of the start of the next instruction bundle after the
 * bundle that fixP is in. This is done by searching through the frags until
 * an instruction with the stop bit is set. That signals the end of the
 * current bundle. The result of is that address + 4 (the size of a single 
 * instruction).
 */
long
md_pcrel_from (fixS *fixP)
{
	offsetT where = fixP->fx_where;
	fragS *frag = fixP->fx_frag;
	while (1)
	{
		if (where >= frag->fr_fix)
		{
			where = 0;
			if (frag->fr_next)
				frag = frag->fr_next;
			else
				break;
		}
		if (bfd_getb32 ((void *) frag->fr_literal + where) & 0x2)
			return frag->fr_address + where + 4;
		where += 4;
	}
	as_fatal("Missing stop bit in assembler output");
	return 0;
}


void
md_apply_fix (fixS *fixP, valueT *valP, segT seg ATTRIBUTE_UNUSED)
{
	void *where;
	unsigned int insn;
	reloc_howto_type *howto;
	if (fixP->fx_addsy != (symbolS *) NULL)
		return;
	if (fixP->fx_r_type != BFD_RELOC_RVEX_BRANCH)
		return;
	howto = bfd_reloc_type_lookup (stdoutput, fixP->fx_r_type);

	where = fixP->fx_frag->fr_literal + fixP->fx_where;
	insn = bfd_getb32 (where);
	insn = (insn & ~howto->dst_mask) |
		(((*valP >> howto->rightshift) << howto->bitpos) & howto->dst_mask);
	bfd_putb32 ((bfd_vma) insn, where);
	fixP->fx_done = 1;
	return;
}

valueT
md_section_align (segT seg, valueT val)
{
	/* Round .text section to a multiple of the minimum alignment for the current issue_width.  */
	if (seg == text_section)
		return (val + ((issue_width*4) -1)) & ~((issue_width*4) -1);
	return val;
}


/* Parse an operand that is machine-specific .*/

void
md_operand (expressionS * exp ATTRIBUTE_UNUSED)
{
	printf("md_operand\n");
	return;
}

static void parse_borrow_string(const char *str)
{
	unsigned int i = 0;
	unsigned int j = 0;
	const char *borrowp = str;

	/* Allocate slot_borrow arrays. */
	slot_borrow = xcalloc(issue_width, sizeof(int *));
	for (i = 0; i < issue_width; i++)
		slot_borrow[i] = xcalloc(issue_width, sizeof(int));

	i = 0;
	j = 0;

	while (*borrowp != '\0')
	{
		char *borrowp_end = NULL;
		if (i >= issue_width || j >= issue_width)
		{
			as_warn (_("Borrow string has more entries than issue_width"));
			break;
		}
		slot_borrow[i][j] = strtol(borrowp, &borrowp_end, 10);
		if (borrowp == borrowp_end)
		{
			as_fatal (_("Error parsing borrow string"));
			return;
		}
		borrowp = borrowp_end;
		if(*borrowp == '.' || *borrowp == '\0')
		{
			slot_borrow[i][j+1] = -1;
			i++;
			j = 0;
		} else
			j++;
		if (*borrowp == '\0')
			break;
		borrowp++;
	}
	/* Fill remaining borrow slots with -1 to indicate that they are not used. */
	for (; i < issue_width; i++)
		slot_borrow[i][0] = -1;
}

void
md_begin (void)
{
	unsigned int i = 0, j = 0;
	/* Set up a hash table for the instructions.  */
	if ((rvex_inst_hash = hash_new ()) == NULL)
		as_fatal (_("Virtual memory exhausted"));

	while (rvex_instruction[i].mnemonic != NULL)
	{
      const char *hashret;
      const char *mnemonic = rvex_instruction[i].mnemonic;

      hashret = hash_insert (rvex_inst_hash, mnemonic,
                             (char *)(rvex_instruction + i));

      if (hashret != NULL && *hashret != '\0')
        as_fatal (_("Can't hash `%s': %s\n"), rvex_instruction[i].mnemonic,
                  *hashret == 0 ? _("(unknown reason)") : hashret);

		/*skip instructions with the same name in the table*/
		do
		{
			++i;
		}
		while (rvex_instruction[i].mnemonic != NULL
				&& (strcmp (rvex_instruction[i].mnemonic, mnemonic) == 0));
	}
	//prepare the nop instruction used to fill empty slots.
	build_nop(&nop_instruction);
	nodes = createList();
	sorted_nodes = xcalloc(issue_width, sizeof(graph_node *));
	filled = xcalloc(issue_width, sizeof(int));

	fu_configuration = xmalloc(issue_width * sizeof(functional_unit_type));
		//= {ALU, ALU|MUL, ALU|MEM, ALU|MUL, ALU, ALU|MUL, ALU|BR, ALU|MUL};
	if (config_str == NULL)
	{
		config_str = "<|config_str|>";
	}
	if (fconfig_str == NULL)
	{
		fconfig_str = "<|fconfig_str|>";
	}
	for (i = 0; i < issue_width; i++)
	{
		char temp[4] = {0,0,0,0};
		temp[0] = config_str[i];
		temp[2] = fconfig_str[i];
		fu_configuration[i] = (functional_unit_type)(
			strtol(temp + 0, NULL, 16) | (strtol(temp + 2, NULL, 32) << 4));
	}

	if (borrow_str == NULL)
	{
		borrow_str = "<|borrow_str|>";
	}

	parse_borrow_string(borrow_str);

	if (debug)
	{
		printf("issue width = %d\n", issue_width);
		for (i = 0; i < issue_width; i++)
		{
			printf("borrow[%d]", i);
			for (j = 0; j < issue_width; j++)
			{
				printf("%d,", slot_borrow[i][j]);
				if (slot_borrow[i][j] == -1)
					break;
			}
			printf("\b\n");
		}
		printf("config = {");
		for (i = 0; i < issue_width; i++)
		{
			printf("%x,", fu_configuration[i]);
		}
		printf("\b}\n");
	}

	linkrelax = 0;
}

void rvex_cleanup(void)
{
	unsigned int i;
	destroyList(nodes);
	xfree(sorted_nodes);
	xfree(filled);
	xfree(fu_configuration);
	for (i = 0; i < issue_width; i++)
		xfree(slot_borrow[i]);
	xfree(slot_borrow);
}


/**
	Parse registers in the form [rbl][0-9]+\.[0-9]+
**/
static void parse_register(argument *arg)
{
	int i = 0;
	char *param = input_line_pointer;

	if (param[0] == 'r')
	{
		arg->type = reg_g;
	}
	else if (param[0] == 'b')
	{
		arg->type = reg_b;
	}
	else if (param[0] == 'l')
	{
		arg->type = reg_l;
	}
	else
	{
		as_bad (_("Invalid register: `%s'"), param);
		return;
	}

	param += 1;

	for (i = 0; ISDIGIT(param[i]); i++)
		;
	if (param[i] == '.')
		param[i] = '\0';
	else
		return;

	arg->reg_cluster = atoi(param);

	param += i + 1;
	for (i = 0; ISDIGIT(param[i]); i++)
		;
	if (i <= 0)
	{
		as_bad (_("Invalid register: `%s'"), param);
		return;
	}

	arg->reg_number = (unsigned) atoi(param);
	input_line_pointer = param + i;
}

#define IS_SHORT_IMMEDIATE(A, INSN)\
(((A) >= (IMM_MIN_VAL)) && ((A) <= (IMM_MAX_VAL)) && ((INSN)->exp.X_op != O_symbol))

#define IS_MEM_STORE(INSTR)\
((INSTR)->mnemonic[0] == 's')

/*
 * Check if this instruction has been detected as the correct instruction.
 * returns 1 for correct and 0 for incorrect.
 */
static int is_correct_instruction(ins *insn)
{
	int i;
	if (insn->nargs != insn->instruction->nargs)
	{
		return 0;
	}
	for (i = 0; i < insn->nargs; i++)
	{
		if (insn->arguments[i].type != insn->instruction->operands[i].op_type)
		{
			return 0;
		}
		else if (insn->arguments[i].type == imm)
		{
			/*If it is a branch immediate, just continue.*/
			if(insn->instruction->operands[i].size == BRANCH_IMM_SIZE)
				continue;
			/*If it is an immediate extension, just continue*/
			if(insn->instruction->operands[i].size == EXTEND_IMM_SIZE)
				continue;
			if(insn->instruction->operands[i].size == SHORTER_IMM_SIZE)
				continue;
			/*If its a long immediate but the instruction type is not:*/
			/*Do we even need those three other ifs?*/
			if ((!IS_SHORT_IMMEDIATE(insn->arguments[i].imm_value, insn)) &&
					(insn->instruction->long_immediate == 0))
			{
				return 0;
			}
		}
	}
	return 1;
}

/*
 * Perform checks on operands for each instruction, also updates the instruction in case of long immediates, or immediates.
 */
static void get_correct_instruction(ins *insn)
{
	int i;
	while (!is_correct_instruction(insn))
	{
		if ((insn->instruction+1)->mnemonic && (strcmp(insn->instruction->mnemonic,
						(insn->instruction+1)->mnemonic) == 0))
		{
			insn->instruction++;
		}
		else
		{
			as_bad (_("Invalid operand for instruction: %s"), insn->instruction->mnemonic);
			return;
		}
	}
	for (i = 0; i < insn->nargs; i++)
		insn->arguments[i].oper = &insn->instruction->operands[i];
}

static void parse_operand(char *param, argument *arg, ins *insn)
{
	char *saved_input = input_line_pointer;
	if (param[0] == '$')
	{
		input_line_pointer = param + 1;
		parse_register(arg);
		if (*input_line_pointer != '\0')
		{
			as_bad (_("Junk after register: `%s'"), param);
			return;
		}
	}
	else
	{
		input_line_pointer = param;
		expression(&(insn->exp));
		arg->type = imm;
		if (*input_line_pointer != '\0')
		{
			as_bad (_("Unexpected input after expression: `%s'"), input_line_pointer);
			return;
		}
		switch (insn->exp.X_op)
		{
			case O_symbol:
				arg->imm_value = (int) insn->exp.X_add_number;
				if (insn->instruction->type == branch)
					insn->rtype = BFD_RELOC_RVEX_BRANCH;
				else
					insn->rtype = BFD_RELOC_RVEX_MOV_L;
				break;
			case O_constant:
				arg->imm_value = (int) insn->exp.X_add_number;
				break;
			default:
				as_bad (_("Unkown expression type"));
				break;
		}
	}
	input_line_pointer = saved_input;
	return;
}

static void parse_operands(char *params, ins *insn)
{
	char *param_start, *param_end;
	char *operands[MAX_OPERANDS];
	int operand_n = 0, i = 0;
	int sq_brace_open = 0, brace_open = 0;
	param_start = param_end = params;

	while (ISSPACE (*param_start) && *param_start != '\0')
		param_start++;
	if (*param_start == '\0') return; //found only whitespace

	while(*param_end != '\0')
	{
		if ((*param_end == '=' || *param_end == ',') && !brace_open)
		{
			*param_end = '\0';
			if(operand_n == MAX_OPERANDS)
			{
				as_bad (_("Too many operands: `%s'"), param_start);
				break;
			}
			operands[operand_n] = param_start;
			operand_n++;
			param_end++;
			param_start = param_end;
			continue;
		}
		if (*param_end == '[')
		{
			sq_brace_open = 1;
			*param_end = ',';
			continue;
		}
		if (*param_end == '(')
		{
			brace_open++;
		}

		if (*param_end == ')')
		{
			brace_open--;
			if(brace_open < 0)
			{
				as_bad(_("Missing matching brackets : `)'"));
				return;
			}
		}
		if (*param_end == ']')
		{
			sq_brace_open--;
			*param_end = '\0';
			if(sq_brace_open < 0)
			{
				as_bad(_("Missing matching brackets : `]'"));
				return;
			}
		}
		param_end++;
	}

	if(operand_n >= MAX_OPERANDS)
	{
		as_bad (_("Too many operands: `%s'"), param_start);
		return;
	}
	operands[operand_n] = param_start;
	operand_n++;
	insn->nargs = operand_n;
	for (i = 0; i < operand_n; i++)
	{
		parse_operand(operands[i], &insn->arguments[i], insn);
	}
	return;
}
static void print_instruction(ins *insn)
{
	int i;
	if (insn == NULL)
		return;
	printf("%s\t", insn->instruction->mnemonic);
	for (i = 0; i < insn->nargs; i++)
	{
		switch(insn->arguments[i].type)
		{
			case reg_g:
				printf("r.%d,", insn->arguments[i].reg_number);
				break;
			case reg_b:
				printf("b.%d,", insn->arguments[i].reg_number);
				break;
			case reg_l:
				printf("l.%d,", insn->arguments[i].reg_number);
				break;
			case imm:
				printf("(");
				if (insn->exp.X_op == O_symbol)
				{
					if (insn->exp.X_add_symbol->sy_flags.sy_local_symbol == 1)
						printf("'unknown local label' + ");
					else
						printf("%s + ", insn->exp.X_add_symbol->bsym->name);
				}
				printf("%d),", insn->arguments[i].imm_value);
				break;
			default:
				break;
		}
	}
	printf("\n");
}

static void insertFlags(unsigned int *syllable, ins *insn)
{
	*syllable |= ((insn->instruction->immediate) << 23);
}

#define IS_LONG_IMM_INSTR(INSN)\
((INSN)->instruction->long_immediate && (INSN)->instruction->immediate)

static void insert_arguments(unsigned int *syllable, ins *insn)
{
	int i, mask;
	for (i = 0; i < insn->nargs; i++)
	{

		if (insn->arguments[i].oper == NULL)//skip arguments that are hardwired such as in return
			continue;
		switch (insn->arguments[i].type)
		{
/* XXX currently there is only 1 lr. The hardware ignores the regnr but it would be nicer to catch and report reg_nr != 0 here */
			case reg_l:
				if (insn->arguments[i].reg_number > (1<<6)-1) 
				{
					as_bad (_("Register number too large %d"), insn->arguments[i].reg_number);
					return;
				}
				*syllable |= (insn->arguments[i].reg_number << insn->arguments[i].oper->shift);
				break;
			case reg_b:
				if (insn->arguments[i].reg_number > (1<<3)-1)
				{
					as_bad (_("Register number too large %d"), insn->arguments[i].reg_number);
					return;
				}
				*syllable |= (insn->arguments[i].reg_number << insn->arguments[i].oper->shift);
				break;
			case reg_g:
				if (insn->arguments[i].oper->size == 0)
					continue;
				if (insn->arguments[i].reg_number > ((unsigned int)1<<insn->arguments[i].oper->size)-1)
				{
					as_bad (_("Register number too large: %d"), insn->arguments[i].reg_number);
					return;
				}
				*syllable |= (insn->arguments[i].reg_number << insn->arguments[i].oper->shift);
				break;
			case imm:
				mask = MAKE_MASK(insn->arguments[i].oper->size);
				*syllable |= ((insn->arguments[i].imm_value & mask) << insn->arguments[i].oper->shift);
				break;
			default:
				break;
		}
	}
}

static unsigned int assemble_instruction(ins *insn)
{
	unsigned int syllable = 0;
	if (insn == NULL)
		return 0;
	INSERT_OPCODE(syllable, insn);
	insertFlags(&syllable, insn);
	insert_arguments(&syllable, insn);

	return syllable;
}

static void init_argument(argument *arg)
{
	arg->imm_value = 0;
	arg->oper = NULL;
	arg->type = 0;
	arg->reg_cluster = 0;
	arg->reg_number = 0;
}

static void init_insn(ins *insn)
{
	int i;
	insn->nargs = 0;
	insn->rtype = BFD_RELOC_NONE;
	insn->instruction = NULL;
	for (i = 0; i < MAX_OPERANDS; i++)
	{
		init_argument(&insn->arguments[i]);
	}
}

/* Outputs a frag containing the assembled instruction pointed to by insn.
 * last indicates that it is the last instruction in the bundle, and the
 * last bit should be set.
 */
static void output_instruction(ins *insn, int last)
{
	char *this_frag = frag_more(4);
	unsigned int syllable = assemble_instruction(insn);
	if (last)
		syllable |= 0x2;

	if(insn->rtype != BFD_RELOC_NONE)
	{
		reloc_howto_type *reloc_howto;
		int size;
		reloc_howto = bfd_reloc_type_lookup (stdoutput, insn->rtype);
		if (!reloc_howto)
			abort ();
		size = bfd_get_reloc_size (reloc_howto);
		if (size < 1 || size > 4)
			abort ();
		fix_new_exp (frag_now, this_frag - frag_now->fr_literal,
				size, &insn->exp, reloc_howto->pc_relative,
				insn->rtype);
	}

	md_number_to_chars (this_frag, (valueT) syllable, 4);
	this_frag += 4;
	frag_now->insn_addr = 0;
	frag_now->has_code = 1;
}

static void build_syllable_follow(ins *insn, ins *insn_src)
{
	insn->nargs = 2;
	insn->cluster = insn_src->cluster;
	insn->instruction = (const inst *) hash_find (rvex_inst_hash, "limmh");
	insn->exp = insn_src->exp;
	argument *arg1 = &insn->arguments[0];
	argument *arg2 = &insn->arguments[1];
	arg1->type = imm;
	arg2->type = imm;
	if (insn->exp.X_op == O_symbol)
	{
		arg2->imm_value = (int) insn->exp.X_add_number;
		insn->rtype = BFD_RELOC_RVEX_MOV_H;
	}
	else
		arg2->imm_value = (int) insn->exp.X_add_number >> SHORT_IMM_SIZE;
}

static void build_nop(ins *insn)
{
	init_insn(insn);
	insn->cluster = 0;
	insn->instruction = (const inst *) hash_find (rvex_inst_hash, "nop");
}

/**
	convert pseudo instruction into the corresponding real instruction
valid pseudo instructions are: mov, mfb, mtb
*/
static void convert_pseudo_instruction(ins *insn)
{
	const inst *instruction = NULL;
	char *save_line_pointer = input_line_pointer;
	if (insn->nargs != insn->instruction->nargs)
	{
		as_bad (_("Invalid number of arguments for instruction %s: %d instead of %d"), insn->instruction->mnemonic,
					insn->nargs, insn->instruction->nargs);
		return;
	}
	if (insn->instruction->type != pseudo)
	{
		return;
	}
	if (streq(insn->instruction->mnemonic, "mov"))
	{
		const char *name;
		if (insn->arguments[0].type == reg_l)
			name = "movtl";
		else if (insn->arguments[1].type == reg_l)
			name = "movfl";
		else
		{
			name = "add";
			insn->arguments[insn->nargs] = insn->arguments[insn->nargs - 1];
			insn->arguments[insn->nargs-1].type = reg_g;
			insn->arguments[insn->nargs-1].reg_number = 0;
			insn->arguments[insn->nargs-1].reg_cluster = insn->cluster;
			insn->nargs += 1;
		}
		instruction = (const inst *) hash_find (rvex_inst_hash, name);
	}
	else if (streq(insn->instruction->mnemonic, "mfb") || streq(insn->instruction->mnemonic, "convbi"))
	{
		instruction = (const inst *) hash_find (rvex_inst_hash, "slctf");
		insn->arguments[insn->nargs].type = reg_g;
		insn->arguments[insn->nargs].reg_number = 0;
		insn->arguments[insn->nargs].reg_cluster = insn->cluster;
		insn->arguments[insn->nargs+1].type = imm;
		insn->arguments[insn->nargs+1].imm_value = 1;
		input_line_pointer = (char *) "1";
		expression(&insn->exp);
		insn->nargs += 2;
	}
	else if (streq(insn->instruction->mnemonic, "mtb") || streq(insn->instruction->mnemonic, "convib"))
	{
		instruction = (const inst *) hash_find (rvex_inst_hash, "cmpne");
		insn->arguments[insn->nargs].type = imm;
		insn->arguments[insn->nargs].imm_value = 0;
		input_line_pointer = (char *) "1";
		expression(&insn->exp);
		insn->nargs += 1;
	}

	input_line_pointer = save_line_pointer;
	if (insn->instruction == NULL)
	{
		as_bad (_("Unknown pseudo instruction: `%s'"), insn->instruction->mnemonic);
		return;
	}
	insn->instruction = instruction;
	return;
}

static unsigned int get_cluster(char **op)
{
	int cluster = 0; //default cluster
	char *param = *op;
	if (param[0] == 'c' && (param[1] < '4' && param[1] >= '0')) //line starts with a cluster designation
	{
		param++; //consume the c
		param[1] = '\0'; //zero-terminate (assumes the cluster nr is followed by whitespace)
		cluster = (unsigned int) atoi (param);
		*op+=3; //consume the value
	}
	return cluster;
}

void
md_assemble (char *op)
{
	char *param;
	char *mnemonic;
	char save2;
	ins *insn;
	graph_node *node;

	node = build_node(nodes->count);
	insn = node->instruction;

	//H TODO: consuming these values could be done more neatly (eating any cluster nr input, removing whitespace)

	insn->cluster = get_cluster(&op);
	param = op;

	mnemonic = param;
	/*skip */
	for (; *param != 0 && !ISSPACE (*param); param++)
		;
	save2 = *param;
	*param = '\0';
	param++;

	insn->instruction = (const inst *) hash_find (rvex_inst_hash, mnemonic);

	if (insn->instruction == NULL)
	{
		as_bad (_("Unknown opcode: `%s'"), mnemonic);
		return;
	}

	if (save2 != '\0')
		parse_operands(param, insn);

	if (insn->instruction->type == pseudo)
		convert_pseudo_instruction(insn);
	if (insn->instruction->type == special)
	{
		destroyNode(node);
		return;
	}
	get_correct_instruction(insn);
	listAddNode(nodes, node);
	if (debug)
		print_instruction(node->instruction);
	/*in the case of a long immediate, generate a syllable follow instruction*/
	if (insn->instruction->long_immediate == 1)
	{
		createFollowNode(node);
		if (debug)
			print_instruction(node->follow_node->instruction);
	}
	return;
}

