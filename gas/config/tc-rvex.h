#ifndef TC_RVEX_H
#define TC_RVEX_H

#define TARGET_FORMAT "elf32-rvex"
#define TARGET_ARCH   bfd_arch_rvex

#define TC_RVEX 1

#define LEX_QM 3

#undef TARGET_BYTES_BIG_ENDIAN
#define TARGET_BYTES_BIG_ENDIAN 1

#define md_number_to_chars      number_to_chars_bigendian

/*this does something to do with long and short jumps*/
#define WORKING_DOT_WORD

extern const char rvex_symbol_chars[];
#define tc_symbol_chars rvex_symbol_chars

void rvex_cleanup(void);
#define md_cleanup rvex_cleanup

extern int rvex_unrecognized_line (char);
extern void rvex_check_label(symbolS *label);
extern void rvex_cons_align(int nbytes);

#define tc_unrecognized_line(ch)	rvex_unrecognized_line (ch)
#define tc_check_label(label)		rvex_check_label(label)
#define md_cons_align(nbytes)		rvex_cons_align(nbytes)

#endif
