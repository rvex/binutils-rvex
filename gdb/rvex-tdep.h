/* Target-dependent code for Xilinx MicroBlaze.

   Copyright (C) 2009-2013 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef RVEX_TDEP_H
#define RVEX_TDEP_H 1


/* Microblaze architecture-specific information.  */
struct gdbarch_tdep
{
};

struct rvex_frame_cache
{
  /* Base address.  */
  CORE_ADDR base;
  CORE_ADDR pc;

  /* Do we have a frame?  */
  int frameless_p;

  /* Frame size.  */
  int framesize;

  /* Frame register.  */
  int fp_regnum;

  /* Offsets to saved registers.  */
  int register_offsets[76];	/* Must match RVEX_NUM_REGS.  */

  /* Table of saved registers.  */
  struct trad_frame_saved_reg *saved_regs;
};

/* Register numbers.  */
enum rvex_regnum 
{
  RVEX_R0_REGNUM,
  RVEX_R1_REGNUM, RVEX_SP_REGNUM = RVEX_R1_REGNUM,
  RVEX_R2_REGNUM,
  RVEX_R3_REGNUM, RVEX_RETVAL_REGNUM = RVEX_R3_REGNUM, RVEX_FIRST_ARGREG = RVEX_R3_REGNUM,
  RVEX_R4_REGNUM,
  RVEX_R5_REGNUM, 
  RVEX_R6_REGNUM,
  RVEX_R7_REGNUM,
  RVEX_R8_REGNUM,
  RVEX_R9_REGNUM,
  RVEX_R10_REGNUM, RVEX_LAST_ARGREG = RVEX_R10_REGNUM,
  RVEX_R11_REGNUM,
  RVEX_R12_REGNUM,
  RVEX_R13_REGNUM,
  RVEX_R14_REGNUM,
  RVEX_R15_REGNUM,
  RVEX_R16_REGNUM,
  RVEX_R17_REGNUM,
  RVEX_R18_REGNUM,
  RVEX_R19_REGNUM,
  RVEX_R20_REGNUM,
  RVEX_R21_REGNUM,
  RVEX_R22_REGNUM,
  RVEX_R23_REGNUM,
  RVEX_R24_REGNUM,
  RVEX_R25_REGNUM,
  RVEX_R26_REGNUM,
  RVEX_R27_REGNUM,
  RVEX_R28_REGNUM,
  RVEX_R29_REGNUM,
  RVEX_R30_REGNUM,
  RVEX_R31_REGNUM,
  RVEX_R32_REGNUM,
  RVEX_R33_REGNUM,
  RVEX_R34_REGNUM,
  RVEX_R35_REGNUM,
  RVEX_R36_REGNUM,
  RVEX_R37_REGNUM,
  RVEX_R38_REGNUM,
  RVEX_R39_REGNUM,
  RVEX_R40_REGNUM,
  RVEX_R41_REGNUM,
  RVEX_R42_REGNUM,
  RVEX_R43_REGNUM,
  RVEX_R44_REGNUM,
  RVEX_R45_REGNUM,
  RVEX_R46_REGNUM,
  RVEX_R47_REGNUM,
  RVEX_R48_REGNUM,
  RVEX_R49_REGNUM,
  RVEX_R50_REGNUM,
  RVEX_R51_REGNUM,
  RVEX_R52_REGNUM,
  RVEX_R53_REGNUM,
  RVEX_R54_REGNUM,
  RVEX_R55_REGNUM,
  RVEX_R56_REGNUM,
  RVEX_R57_REGNUM,
  RVEX_R58_REGNUM,
  RVEX_R59_REGNUM,
  RVEX_R60_REGNUM,
  RVEX_R61_REGNUM,
  RVEX_R62_REGNUM, RVEX_FP_REGNUM = RVEX_R62_REGNUM,
  RVEX_R63_REGNUM,
  RVEX_B0_REGNUM,
  RVEX_B1_REGNUM,
  RVEX_B2_REGNUM,
  RVEX_B3_REGNUM,
  RVEX_B4_REGNUM,
  RVEX_B5_REGNUM,
  RVEX_B6_REGNUM,
  RVEX_B7_REGNUM,
  RVEX_LR_REGNUM,
  RVEX_PC_REGNUM,
  RVEX_CCR_REGNUM,
  RVEX_SCCR_REGNUM,
  RVEX_TC_REGNUM,
  RVEX_TP_REGNUM,
  RVEX_TA_REGNUM,
  RVEX_TH_REGNUM,
  RVEX_PH_REGNUM
};

#define INST_WORD_SIZE  4 

/* All registers are 32 bits.  */
#define RVEX_REGISTER_SIZE 4

/* RVEX_BREAKPOINT defines the breakpoint that should be used.
   Only used for native debugging.  */
#define RVEX_BREAKPOINT {0xb9, 0xcc, 0x00, 0x60}
/* TRAP 127 halts the debugger */
#define VEX_TRAP_INSTR  {	0x90,0x80,0x01,0xfc, \
							0x60,0x00,0x00,0x00, \
							0x60,0x00,0x00,0x00, \
							0x60,0x00,0x00,0x02}

#endif /* microblaze-tdep.h */
