#ifndef _RVEX_H_
#define _RVEX_H_

#define MAX_OPERANDS 5
#define MIN_ISSUE_WIDTH 2
#define LOG_MAX_WIDTH 3
#define LOG_MIN_WIDTH 1

#define INSTRUCTION_SIZE 32
#define OPCODE_SIZE 8

#define IMM_MIN_VAL (-256)
#define IMM_MAX_VAL (255)

#define SHORT_IMM_SIZE (9)
#define SHORTER_IMM_SIZE (3)
#define BRANCH_IMM_SIZE (19)
#define EXTEND_IMM_SIZE (23)

#define streq(A,B) (strcmp (A, B) == 0)

typedef enum{
	NONE   = 0x000,
	ALU    = 0x001,
	MUL    = 0x002,
	MEM    = 0x004,
	BR     = 0x008,
	FADD   = 0x010,
	FMUL   = 0x020,
	FCMP   = 0x040,
	CONVIF = 0x080,
	CONVFI = 0x100,
	ALL    = 0x1FF
} functional_unit_type;

typedef enum
{
	dummy,
	/* N-bit signed immediate.  */
	imm,
	/*general register*/
	reg_g, reg_b, reg_l,
	/* Not an operand.  */
	nulloperand,
	/* Maximum supported operand.  */
	MAX_OPRD
}
operand_type;

typedef enum{
	none,
	read_access,
	write_access
} access_type;

typedef enum{
	arith,
	mult,
	mem,
	branch,
	fadd,
	fmul,
	fcmp,
	convif,
	convfi,
	addcg,
	pseudo,
	follow,
	special,
	unknown_inst
} instruction_type;

typedef struct
  {
    /* Operand type.  */
    operand_type op_type;
    /* Operand location within the opcode.  */
    unsigned int shift;
	/* source register, destination register, or none */
	access_type access;
	int size;
  }
operand_desc;

typedef struct
  {
    int imm_value;
	const operand_desc *oper;
	
    operand_type type;
    /* Size of the argument (in bits) required to represent.  */
	unsigned int reg_cluster;
	unsigned int reg_number;
  }
argument;

typedef struct
{
	/* Name.  */
	const char *mnemonic;
	/* Size (in double words).  */
	int nargs;
	/* Constant prefix (matched by the disassembler).  */
	unsigned long match;  /* ie opcode */
	/* Match size (in bits).  */
	/* MASK: if( (i & match_bits) == match ) then match */
	int match_bits;
	/* Attributes.  */
	unsigned char long_immediate;
	unsigned char immediate;
	unsigned int flags;
	instruction_type type;
	/* Operands (always last, so unreferenced operands are initialized).  */
	operand_desc operands[MAX_OPERANDS];
}
inst;

#define INSERT_OPCODE(A, INS)\
(A) |= (((INS)->instruction->match) << (INSTRUCTION_SIZE - OPCODE_SIZE))

#define IS_LONG_IMM_INSTR(INSN)\
((INSN)->instruction->long_immediate && (INSN)->instruction->immediate)

#define IS_FOLLOW_INSTR(INSN)\
	((INSN)->instruction->type == follow)

#define INSERT_FLAGS(SYL, INSN)\
((SYL) |= ((((((INSN)->instruction->flags) << 1)\
		| (INSN)->instruction->long_immediate) << 1)\
		| (INSN)->instruction->immediate) << 23)

#define MAKE_MASK(SIZE)\
	((1<<SIZE)-1)

typedef struct{
	int nargs;
	int cluster;
	const inst *instruction;
#ifdef TC_RVEX
	expressionS exp;
	bfd_reloc_code_real_type rtype;
#endif
	argument arguments[MAX_OPERANDS];
} ins;

typedef struct graph_node graph_node;
typedef struct node_list node_list;

struct node_list{
	graph_node **list;
	unsigned int size;
	unsigned int count;
};

struct graph_node{
	ins *instruction;
	int start;
	int end;
	int follow;
	graph_node *follow_node;
	node_list *parent;
	node_list *child;
	int id;
	int scheduled;
};

extern const inst rvex_instruction[];
extern const int rvex_num_opcodes;
#endif
