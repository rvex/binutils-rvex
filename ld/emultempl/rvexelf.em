
fragment <<EOF
#include "ldctor.h"

static bfd_boolean
hook_in_stub (lang_statement_list_type add, sec_ptr input_section, lang_statement_union_type **lp)
{
  lang_statement_union_type *l;
  bfd_boolean ret;

  for (; (l = *lp) != NULL; lp = &l->header.next)
    {
      switch (l->header.type)
	{
	case lang_constructors_statement_enum:
	  ret = hook_in_stub (add, input_section, &constructor_list.head);
	  if (ret)
	    return ret;
	  break;

	case lang_output_section_statement_enum:
	  ret = hook_in_stub (add, input_section,
			      &l->output_section_statement.children.head);
	  if (ret)
	    return ret;
	  break;

	case lang_wild_statement_enum:
	  ret = hook_in_stub (add, input_section, &l->wild_statement.children.head);
	  if (ret)
	    return ret;
	  break;

	case lang_group_statement_enum:
	  ret = hook_in_stub (add, input_section, &l->group_statement.children.head);
	  if (ret)
	    return ret;
	  break;

	case lang_input_section_enum:

	  if (l->input_section.section == input_section)
	    {
	      /* We've found our section.  Insert the stub immediately
		 after its associated input section.  */
	      *(add.tail) = l->header.next;
	      l->header.next = add.head;
	      return TRUE;
	    }
	  break;

	case lang_data_statement_enum:
	case lang_reloc_statement_enum:
	case lang_object_symbols_statement_enum:
	case lang_output_statement_enum:
	case lang_target_statement_enum:
	case lang_input_statement_enum:
	case lang_assignment_statement_enum:
	case lang_padding_statement_enum:
	case lang_address_statement_enum:
	case lang_fill_statement_enum:
	  break;

	default:
	  FAIL ();
	  break;
	}
    }
  return FALSE;
}

static lang_input_statement_type *stub_file = NULL;

static sec_ptr rvexelf_create_stub_section(bfd *abfd, bfd *bfd2 ATTRIBUTE_UNUSED, sec_ptr sec)
{
	char *bla = bfd_get_unique_section_name(abfd, ".stub", NULL);
	sec_ptr stub_sec = bfd_make_section_anyway_with_flags(abfd, bla, (SEC_ALLOC | SEC_LOAD | SEC_HAS_CONTENTS | SEC_IN_MEMORY | SEC_CODE | SEC_READONLY | SEC_KEEP | SEC_RELOC));
    bfd_set_section_alignment(abfd, stub_sec, 3);
	sec_ptr sreloc = _bfd_elf_make_dynamic_reloc_section (stub_sec,
			abfd,
			3,
			abfd,
			TRUE);

	lang_output_section_statement_type *os;
	os = lang_output_section_get (sec->output_section);

	lang_statement_list_type add;
	lang_list_init (&add);
	lang_add_section (&add, stub_sec, NULL, os);
	lang_add_section (&add, sreloc, NULL, os);
	if (add.head == NULL)
		printf("error\n");
	if(!hook_in_stub(add, sec, &os->children.head))
    {
		printf("hook_in_stub error!!!\n");
    }
	return stub_sec;
}

static int
rvexelf_after_allocation_check_reloc(void)
{
	int rerun = 0;
    LANG_FOR_EACH_INPUT_STATEMENT (is)
    {
        bfd *abfd = is->the_bfd;
        asection *sec;

        if ((abfd->flags & (EXEC_P | DYNAMIC)) != 0)
            continue;

        for (sec = abfd->sections; sec != NULL; sec = sec->next)
        {
            asection *out_sec = sec->output_section;
            if (out_sec
                    && elf_section_data (sec)
                    && elf_section_type (sec) == SHT_PROGBITS
                    && (elf_section_flags (sec) & SHF_EXECINSTR) != 0
                    && (sec->flags & SEC_EXCLUDE) == 0
                    && sec->sec_info_type != SEC_INFO_TYPE_JUST_SYMS
                    && out_sec != bfd_abs_section_ptr)
            {
				if (stub_file == NULL)
				{
					stub_file = lang_add_input_file ("linker stubs",
							lang_input_file_is_fake_enum,
							NULL);
					stub_file->the_bfd = bfd_create ("linker stubs", link_info.output_bfd);
					if (stub_file->the_bfd == NULL
							|| ! bfd_set_arch_mach (stub_file->the_bfd,
								bfd_get_arch (link_info.output_bfd),
								bfd_get_mach (link_info.output_bfd)))
					{
						einfo ("%X%P: can not create BFD %E\n");
						return rerun;
					}

					stub_file->the_bfd->flags |= BFD_LINKER_CREATED;
					ldlang_add_file (stub_file);
					elf_elfheader (stub_file->the_bfd)->e_ident[EI_CLASS] = ELFCLASS32;
				}
                if (check_relocs(abfd, stub_file->the_bfd, link_info.output_bfd, 
                        sec, &link_info, rvexelf_create_stub_section))
					rerun = 1;
            }
        }
    }
	return rerun;
}

static void
rvexelf_after_allocation (void)
{
    gldelf32rvex_after_allocation();
    rvexelf_after_allocation_check_reloc();
    gldelf32rvex_map_segments (TRUE);
    rvex_finallize_relocs(stub_file->the_bfd, &link_info);
}

EOF

LDEMUL_AFTER_ALLOCATION=rvexelf_after_allocation
