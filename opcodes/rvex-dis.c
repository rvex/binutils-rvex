#include "sysdep.h"
#include "dis-asm.h"
#include "opcode/rvex.h"
#include "libiberty.h"

typedef int instr_t;
struct dissassemble_data{
	int index;
	int follow[8];
	int value[8];
};

static struct dissassemble_data data;
static int new_bundle = 1;
static int start = 1;
static int padding = 0;
static bfd_vma address = 0;

static int is_last(instr_t instruction, int ins_index)
{
	if (!padding)
	{
		if (instruction & 0x2)
			return 1;
	}
	else if (ins_index == padding - 1)
		return 1;
	return 0;
}

static instr_t get_vex_instruction(bfd_vma memaddr, struct disassemble_info *info)
{
	int status;
	instr_t insn = 0;
	bfd_byte buffer[4];
	status = info->read_memory_func (memaddr, buffer, 4, info);
	if (status == 0)
		insn = (instr_t) bfd_getb32 (buffer);
	return insn;
}

static int sign_extend(int value, int size)
{
	return (value << (32 - size)) >> (32 - size);
}

static int get_value(operand_desc desc, instr_t insn)
{
	int value = 0;
	value = insn >> desc.shift;
	switch(desc.op_type)
	{
		case imm:
			value &= MAKE_MASK(desc.size);
			if (!(desc.size == SHORTER_IMM_SIZE))
			{
				if (data.follow[data.index])
					value |= (data.value[data.index] << desc.size);
				else
					value = sign_extend(value, desc.size);
			}
			break;
		case reg_g:
			value &= 0x3f;
			break;
		case reg_l:
			value &= 0x3f;
			break;
		case reg_b:
			value &= 0x7;
			break;
		default:
			break;
	}
	return value;
}

static void print_operand(const inst *instruction, operand_desc desc, instr_t insn, struct disassemble_info *info)
{
	int value;
	value = insn >> desc.shift;
	switch(desc.op_type)
	{
		case imm:
			value &= MAKE_MASK(desc.size);
			if (!(desc.size == SHORTER_IMM_SIZE))
			{
				if (data.follow[data.index])
					value |= (data.value[data.index] << desc.size);
				else
					value = sign_extend(value, desc.size);
			}
			if (instruction->type == branch && instruction->mnemonic[0] != 'r' && instruction->mnemonic[0] != 't') //not an address if the instruction is a return or a trap
				info->print_address_func (address+(value<<3), info);
			else
				info->fprintf_func (info->stream,"0x%x", value);
			break;
		case reg_g:
			value &= 0x3f;
			info->fprintf_func (info->stream,"r0.%d", value);
			break;
		case reg_l:
			value &= 0x3f;
			info->fprintf_func (info->stream,"l0.0");
			break;
		case reg_b:
			value &= 0x7;
			info->fprintf_func (info->stream,"b0.%d", value);
			break;
		default:
			break;
	}
}

/* Prints out all the arguments for the instruction.
 * The first dest_count are taken as destination arguments.
 */
static void printOperands(const inst *instruction, instr_t insn, struct disassemble_info *info, int dest_count)
{
	int i;
	if (instruction->nargs == 0)
		return;
	print_operand(instruction, instruction->operands[0], insn, info);
	for (i = 1; i < instruction->nargs; i++)
	{
		/*seperate the operands by '=' after dest_count operands*/
		if (i == dest_count)
			info->fprintf_func (info->stream," = ");
		else
			info->fprintf_func (info->stream,", ");
		print_operand(instruction, instruction->operands[i], insn, info);
	}
}

static void print_arguments(const inst *instruction, instr_t insn, struct disassemble_info *info)
{
	info->fprintf_func (info->stream,"\t");
	switch (instruction->type)
	{
        // Transforming a memory instruction
		case mem:
            // Load or store branch register to memory
			if (strcmp("stbr", instruction->mnemonic) == 0 ||
					strcmp("ldbr", instruction->mnemonic) == 0)
			{
				if (get_value (instruction->operands[1], insn) == 0)
					info->print_address_func (get_value(instruction->operands[0], insn), info);
				else
					print_operand(instruction, instruction->operands[0], insn, info);
				info->fprintf_func (info->stream,"[");
				print_operand(instruction, instruction->operands[1], insn, info);
				info->fprintf_func (info->stream,"]");
				break;
			}
            // If it is the stwl instruction
            if (strcmp("stwl", instruction->mnemonic) == 0){
                print_operand(instruction, instruction->operands[0], insn, info);
                info->fprintf_func(info->stream,", ");
				print_operand(instruction, instruction->operands[1], insn, info);
				info->fprintf_func (info->stream,"[");
				print_operand(instruction, instruction->operands[2], insn, info);
				info->fprintf_func (info->stream,"]");
				info->fprintf_func (info->stream," = ");
				print_operand(instruction, instruction->operands[3], insn, info);
            // If it is a load instruction
            } else if (instruction->mnemonic[0] == 'l'){
				print_operand(instruction, instruction->operands[0], insn, info);
				info->fprintf_func (info->stream," = ");
				if (get_value (instruction->operands[2], insn) == 0)
					info->print_address_func (get_value(instruction->operands[1], insn), info);
				else
					print_operand(instruction, instruction->operands[1], insn, info);
				info->fprintf_func (info->stream,"[");
				print_operand(instruction, instruction->operands[2], insn, info);
				info->fprintf_func (info->stream,"]");
            // If it is a store instruction
			} else if (instruction->mnemonic[0] == 's') {
				if (get_value (instruction->operands[1], insn) == 0)
					info->print_address_func (get_value(instruction->operands[0], insn), info);
				else
					print_operand(instruction, instruction->operands[0], insn, info);
				info->fprintf_func (info->stream,"[");
				print_operand(instruction, instruction->operands[1], insn, info);
				info->fprintf_func (info->stream,"]");
				info->fprintf_func (info->stream," = ");
				print_operand(instruction, instruction->operands[2], insn, info);
			}
			break;
		case mult:
		case arith:
		case fadd:
		case fmul:
		case fcmp:
		case convif:
		case convfi:
			printOperands(instruction, insn, info, 1);
			break;
		case addcg:
			printOperands(instruction, insn, info, 2);
			break;
		case follow:
			printOperands(instruction, insn, info, 0);
			break;
		case branch:
			if (instruction->mnemonic[0] == 'b')//br brf instructions
			{
				printOperands(instruction, insn, info, 0);
				break;
			}
			if (instruction->mnemonic[0] == 'g')//goto
			{
				printOperands(instruction, insn, info, 0);
				break;
			}
			if (instruction->mnemonic[0] == 'c')//call
			{
				printOperands(instruction, insn, info, 1);
				break;
			}
			if (instruction->mnemonic[0] == 'r')
			{
				if (instruction->mnemonic[1] == 'e')//return
				{
					info->fprintf_func (info->stream,"r0.1 = r0.1, ");
					print_operand(instruction, instruction->operands[2], insn, info);
					info->fprintf_func (info->stream,", l0.0");
				}
				else if (instruction->mnemonic[1] == 'f')//rfi
				{
					info->fprintf_func (info->stream,"r0.1 = r0.1, ");
					print_operand(instruction, instruction->operands[2], insn, info);
				}
				break;
			}
			if (instruction->mnemonic[0] == 't')//trap
			{
				info->fprintf_func (info->stream,"Cause =  ");
				print_operand(instruction, instruction->operands[0], insn, info);
				info->fprintf_func (info->stream,", Argument =  ");
				print_operand(instruction, instruction->operands[1], insn, info);
				break;
			}
		default:
			break;
	}
}

static const inst *match_opcode (instr_t insn)
{
	int i;
	unsigned int opcode = insn >> (32 - OPCODE_SIZE);
	const inst *instruction;
	
	for (i = 0; i < rvex_num_opcodes - 1; i++)
	{
		instruction = &rvex_instruction[i];
		if ((opcode & instruction->match_bits) == instruction->match)
		{
			switch (instruction->type)
			{
				case addcg:
				case mult:
				case arith:
				case mem:
				case fadd:
				case fmul:
				case fcmp:
				case convif:
				case convfi:
					if (((insn >> 23) & 1) == instruction->immediate)
					{
						return instruction;
					}
					break;
				default:
					return instruction;
			}
		}
	}
	return &rvex_instruction[rvex_num_opcodes - 1];
}

static void startNewBundle(bfd_vma memaddr, struct disassemble_info *info)
{
	int i;
	instr_t insn[8];
	const inst *instruction[8];
	data.index = 0;
	new_bundle = 0;
	for (i = 0; i < 8; i++)
	{
		data.follow[i] = 0;
		data.value[i] = 0;
	}
	for (i = 0; i < 8; i++)
	{
		insn[i] = get_vex_instruction(memaddr+(i*4), info);
		instruction[i] = match_opcode(insn[i]);
		if (is_last(insn[i], i))
		{
			address = memaddr+((i+1)*4);
			break;
		}
	}
	if (i == 8)
			address = memaddr+(i*4);
	for (i = 0; i < 8; i++)
	{
		if (instruction[i]->type == follow)
		{
			int shift = instruction[i]->operands[0].shift;
			int size = instruction[i]->operands[0].size;
			int ins_index = (insn[i] >> shift) & MAKE_MASK(size);
			data.follow[ins_index] = 1;
			data.value[ins_index] = insn[i] >> instruction[i]->operands[1].shift;
		}
		if (is_last(insn[i], i))
			break;
	}
}

int print_insn_rvex (bfd_vma memaddr, struct disassemble_info *info)
{
	instr_t insn;
	const inst *instruction;
	char *arg;
	insn = get_vex_instruction(memaddr, info);
	instruction = match_opcode(insn);
	if (start)
	{
		start = 0;
		arg = info->disassembler_options;
		if (arg && arg[0] == 'p')
		{
			if (arg[1] == '4')
				padding = 4;
			else if (arg[1] == '8')
				padding = 8;
		}
	}

	if (new_bundle)
		startNewBundle(memaddr, info);

	if(instruction->mnemonic != NULL)
	{
		info->fprintf_func (info->stream,"%s", instruction->mnemonic);
		print_arguments(instruction, insn, info);
		if (is_last(insn, data.index))
		{
			info->fprintf_func (info->stream,";;");
			new_bundle = 1;
		}
		data.index++;
	}

	return 4;
}

