#include "libiberty.h"
#include "opcode/rvex.h"

#define DEST_REGG {reg_g, 17, write_access, 6}
#define DEST_REGL {reg_l, 17, write_access, 6}
#define DEST1_REGB {reg_b, 17, write_access, 3}
#define DEST2_REGB {reg_b, 2, write_access, 3}
#define SRC1_REGG {reg_g, 11, read_access, 6}
#define SRC2_REGG {reg_g, 5, read_access, 6}
#define SRC2_REGL {reg_l, 5, read_access, 6}
#define SRC3_REGG {reg_g, 17, read_access, 6}
#define SRC3_REGL {reg_l, 17, read_access, 6}
#define SRC1_REGB {reg_b, 24, read_access, 3}
#define SRC2_REGB {reg_b, 2, read_access, 3}
#define SHORT_IMM {imm, 2, none, SHORT_IMM_SIZE}
#define SHORTER_IMM {imm, 25, none, SHORTER_IMM_SIZE}
#define LONG2_IMM {imm, 2, none, EXTEND_IMM_SIZE}
#define LONG3_IMM {imm, 5, none, BRANCH_IMM_SIZE}
#define NO_OPERAND {nulloperand, 0, none, 0}
#define SRC_REGL_F {reg_l, 0, read_access, 0}
#define DST_REGL_F {reg_l, 0, write_access, 0}
#define SRC_REGG_F {reg_g, 0, read_access, 0}
#define DST_REGG_F {reg_g, 0, write_access, 0}

const inst rvex_instruction[] =
{
	#define FOLLOW_INST\
	{"limmh", 2, 0x80, 0xf0, 0, 0, 0, follow, {SHORTER_IMM, LONG2_IMM}}

	FOLLOW_INST,
	#define SPECIAL_INST(NAME, OPC) \
	{NAME, 0, OPC, 0xff, 0, 0, 0, special, {NO_OPERAND}}

	SPECIAL_INST("nop", 96),
	/* instructions for ALU, Mul, Mem */
	#define  ARITH_INST(NAME, OPC)                             \
	{NAME, 3, OPC, 0xff, 0, 0, 0, arith, {DEST_REGG, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, arith, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, arith, {DEST_REGG, SRC1_REGG, SHORT_IMM}}

	#define  ARITH_SWAP_INST(NAME, OPC)                             \
	{NAME, 3, OPC, 0xff, 0, 0, 0, arith, {DEST_REGG, SRC2_REGG, SRC1_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, arith, {DEST_REGG, SHORT_IMM, SRC1_REGG}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, arith, {DEST_REGG, SHORT_IMM, SRC1_REGG}}

	#define PSEUDO_INST(NAME, NARGS)\
	{NAME, NARGS, -1, 0xff, 0, 0, 0, pseudo, {NO_OPERAND}}

	#define  MULT_INST(NAME, OPC)                             \
	{NAME, 3, OPC, 0xff, 0, 0, 0, mult, {DEST_REGG, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, mult, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, mult, {DEST_REGG, SRC1_REGG, SHORT_IMM}}

	MULT_INST("mpyll", 0),
	MULT_INST("mpyllu", 1),
	MULT_INST("mpylh", 2),
	MULT_INST("mpylhu", 3),
	MULT_INST("mpyhh", 4),
	MULT_INST("mpyhhu", 5),
	MULT_INST("mpyl", 6),
	MULT_INST("mpylu", 7),
	MULT_INST("mpyh", 8),
	MULT_INST("mpyhu", 9),
	MULT_INST("mpyhs", 10),

	#define MOV_TO_LINK_INST(NAME, OPC)\
	{NAME, 2, OPC, 0xff, 0, 0, 0, arith, {DEST_REGL, SRC2_REGG}},\
	{NAME, 2, OPC, 0xff, 0, 1, 0, arith, {DEST_REGL, SHORT_IMM}},\
	{NAME, 2, OPC, 0xff, 1, 1, 0, arith, {DEST_REGL, SHORT_IMM}}

	#define MOV_FROM_LINK_INST(NAME, OPC)\
	{NAME, 2, OPC, 0xff, 0, 0, 0, arith, {DEST_REGG, SRC2_REGL}}

	MOV_TO_LINK_INST("movtl", 11),
	MOV_FROM_LINK_INST("movfl", 12),

	#define MEM_LOAD_INST(NAME, OPC)\
	{NAME, 3, OPC, 0xff, 0, 1, 0, mem, {DEST_REGG, SHORT_IMM, SRC1_REGG}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, mem, {DEST_REGG, SHORT_IMM, SRC1_REGG}}

	#define MEM_LOADB_INST(NAME, OPC)\
	{NAME, 2, OPC, 0xff, 0, 1, 0, mem, {SHORT_IMM, SRC1_REGG}},\
	{NAME, 2, OPC, 0xff, 1, 1, 0, mem, {SHORT_IMM, SRC1_REGG}}


	#define MEM_LOAD_LINK_INST(NAME, OPC)\
	{NAME, 3, OPC, 0xff, 0, 1, 0, mem, {DEST_REGL, SHORT_IMM, SRC1_REGG}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, mem, {DEST_REGL, SHORT_IMM, SRC1_REGG}}

	#define MEM_STORE_INST(NAME, OPC)\
	{NAME, 3, OPC, 0xff, 0, 1, 0, mem, {SHORT_IMM, SRC1_REGG, SRC3_REGG}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, mem, {SHORT_IMM, SRC1_REGG, SRC3_REGG}}

	#define MEM_STOREB_INST(NAME, OPC)\
	{NAME, 2, OPC, 0xff, 0, 1, 0, mem, {SHORT_IMM, SRC1_REGG}},\
	{NAME, 2, OPC, 0xff, 1, 1, 0, mem, {SHORT_IMM, SRC1_REGG}}

	#define MEM_STORE_LINK_INST(NAME, OPC)\
	{NAME, 3, OPC, 0xff, 0, 1, 0, mem, {SHORT_IMM, SRC1_REGG, SRC3_REGL}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, mem, {SHORT_IMM, SRC1_REGG, SRC3_REGL}}

	MEM_LOAD_INST("ldw", 16),
	MEM_LOAD_LINK_INST("ldw", 13),
	MEM_LOAD_INST("ldw.d", 16),
	MEM_LOAD_LINK_INST("ldw.d", 13),
	MEM_LOAD_INST("ldh", 17),
	MEM_LOAD_INST("ldh.d", 17),
	MEM_LOAD_INST("ldhu", 18),
	MEM_LOAD_INST("ldhu.d", 18),
	MEM_LOAD_INST("ldb", 19),
	MEM_LOAD_INST("ldb.d", 19),
	MEM_LOAD_INST("ldbu", 20),
	MEM_LOAD_INST("ldbu.d", 20),
	MEM_STORE_INST("stw", 21),
	MEM_STORE_LINK_INST("stw", 14),
	MEM_STORE_INST("sth", 22),
	MEM_STORE_INST("stb", 23),

	MEM_LOADB_INST("ldbr", 46),
	MEM_STOREB_INST("stbr", 47),

	ARITH_INST("shr", 24),
	ARITH_INST("shru", 25),
	ARITH_SWAP_INST("sub", 26),

	#define ARITH2_INST(NAME, OPC) \
	{NAME, 2, OPC, 0xff, 0, 0, 0, arith, {DEST_REGG, SRC1_REGG}}
	ARITH2_INST("sxtb", 27),
	ARITH2_INST("sxth", 28),
	ARITH2_INST("zxtb", 29),
	ARITH2_INST("zxth", 30),

	ARITH_INST("xor", 31),

	#define BRANCH_INSTR(NAME, OPC, OP1, OP2, NARGS)\
	{NAME, NARGS, OPC, 0xff, 0, 0, 0, branch, {OP1, OP2}}
	BRANCH_INSTR("goto", 32, LONG3_IMM, NO_OPERAND, 1),
	BRANCH_INSTR("goto", 33, SRC_REGL_F, NO_OPERAND, 1),
	BRANCH_INSTR("call", 34, DST_REGL_F, LONG3_IMM, 2),
	BRANCH_INSTR("call", 35, DST_REGL_F, SRC_REGL_F, 2),
	BRANCH_INSTR("br", 36, SRC2_REGB, LONG3_IMM, 2),
	BRANCH_INSTR("brf", 37, SRC2_REGB, LONG3_IMM, 2),
	//BRANCH_INSTR("return", 38, LONG3_IMM, NO_OPERAND, 4),
	{"return", 4, 38, 0xff, 0, 0, 0, branch, {DST_REGG_F, SRC_REGG_F, LONG3_IMM, SRC_REGL_F}},
	{"return", 1, 38, 0xff, 0, 0, 0, branch, {SRC_REGL_F}},
	{"rfi", 3, 39, 0xff, 0, 0, 0, branch, {DST_REGG_F, SRC_REGG_F, LONG3_IMM}},
	BRANCH_INSTR("rfi", 39, NO_OPERAND, NO_OPERAND, 0),
	BRANCH_INSTR("stop", 40, NO_OPERAND, NO_OPERAND, 0),

	{"send", 2, 42, 0xff, 0, 0, 0, unknown_inst, {SRC1_REGG, SHORT_IMM}},
	{"recv", 2, 42, 0xff, 0, 0, 0, unknown_inst, {DEST_REGG, SHORT_IMM}},

	#define SPECIAL4_IMM_INST(NAME, OPC)\
	{NAME, 4, OPC, 0xf8, 0, 0, 0, arith, {DEST_REGG, SRC1_REGB, SRC1_REGG, SRC2_REGG}},\
	{NAME, 4, OPC, 0xf8, 0, 1, 0, arith, {DEST_REGG, SRC1_REGB, SRC1_REGG, SHORT_IMM}},\
	{NAME, 4, OPC, 0xf8, 1, 1, 0, arith, {DEST_REGG, SRC1_REGB, SRC1_REGG, SHORT_IMM}},\
	{NAME, 4, OPC, 0xf8, 0, 0, 0, arith, {DEST_REGL, SRC1_REGB, SRC1_REGG, SRC2_REGG}},\
	{NAME, 4, OPC, 0xf8, 0, 1, 0, arith, {DEST_REGL, SRC1_REGB, SRC1_REGG, SHORT_IMM}},\
	{NAME, 4, OPC, 0xf8, 1, 1, 0, arith, {DEST_REGL, SRC1_REGB, SRC1_REGG, SHORT_IMM}}
	//The last 3 entries are to be able to parse slct instructions targeting the link register.
	//Open64 will sometimes generate these because of an optimization I have been unable to find.
	//This instruction will be split into a normal slct and a mtl insn by fix_select_to_lr()


	SPECIAL4_IMM_INST("slctf", 48),
	PSEUDO_INST("mfb", 2),
	PSEUDO_INST("convbi", 2),
	SPECIAL4_IMM_INST("slct", 56),

	#define LOGIC_INST(NAME, OPC)\
	{NAME, 3, OPC, 0xff, 0, 0, 0, arith, {DEST_REGG, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, arith, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, arith, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, (OPC+1), 0xff, 0, 0, 0, arith, {DEST1_REGB, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, (OPC+1), 0xff, 0, 1, 0, arith, {DEST1_REGB, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, (OPC+1), 0xff, 1, 1, 0, arith, {DEST1_REGB, SRC1_REGG, SHORT_IMM}}

	LOGIC_INST("cmpeq", 64),
	PSEUDO_INST("mtb", 2),
	PSEUDO_INST("convib", 2),
	LOGIC_INST("cmpge", 66),
	LOGIC_INST("cmpgeu", 68),
	LOGIC_INST("cmpgt", 70),
	LOGIC_INST("cmpgtu", 72),
	LOGIC_INST("cmple", 74),
	LOGIC_INST("cmpleu", 76),
	LOGIC_INST("cmplt", 78),
	LOGIC_INST("cmpltu", 80),
	LOGIC_INST("cmpne", 82),
	LOGIC_INST("tbit", 92),
	LOGIC_INST("tbitf", 94),


	LOGIC_INST("nandl", 84),
	LOGIC_INST("norl", 86),
	LOGIC_INST("orl", 88),
	LOGIC_INST("andl", 90),



	ARITH_INST("add", 98),
	PSEUDO_INST("mov", 2),
	ARITH_INST("and", 99),
	ARITH_INST("andc", 100),
	ARITH_INST("max", 101),
	ARITH_INST("maxu", 102),
	ARITH_INST("min", 103),
	ARITH_INST("minu", 104),
	ARITH_INST("or", 105),
	ARITH_INST("orc", 106),
	ARITH_INST("sh1add", 107),
	ARITH_INST("sh2add", 108),
	ARITH_INST("sh3add", 109),
	ARITH_INST("sh4add", 110),
	ARITH_INST("shl", 111),
	ARITH_INST("sbit", 44),
	ARITH_INST("sbitf", 45),

	{"divs", 5, 112, 0xf8, 0, 0, 0, addcg, {DEST_REGG, DEST2_REGB, SRC1_REGG, SRC2_REGG, SRC1_REGB}},
	{"addcg", 5, 120, 0xf8, 0, 0, 0, addcg, {DEST_REGG, DEST2_REGB, SRC1_REGG, SRC2_REGG, SRC1_REGB}},
	
	/* Extensions and other special instructions: */
	{"trap", 2, 0x90, 0xff, 0, 1, 0, branch, {SHORT_IMM, SRC1_REGG}},
	{"trap", 1, 0x90, 0xff, 0, 1, 0, branch, {SHORT_IMM}}, /* If not given, trap argument is $r0.0 */

	/*ST200 extension clz: */
	ARITH2_INST("clz", 0x91),

	/*ST200 extension muls: */
	MULT_INST("mpylhus", 0x92),
	MULT_INST("mpyhhs", 0x93),

	MEM_STORE_INST("xch", 0x94),

	/* Floating point insns */
	#define FP_CONV(NAME, OPC, TYPE)\
	{NAME, 2, OPC, 0xff, 0, 0, 0, TYPE, {DEST_REGG, SRC1_REGG}}
	FP_CONV("convif", 0x95, convif),
	FP_CONV("convfi", 0x96, convfi),
	
	#define FP_INST(NAME, OPC, TYPE) \
	{NAME, 3, OPC, 0xff, 0, 0, 0, TYPE, {DEST_REGG, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, TYPE, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, TYPE, {DEST_REGG, SRC1_REGG, SHORT_IMM}}
	FP_INST("addf", 0x97, fadd),
	FP_INST("subf", 0x98, fadd),
	FP_INST("mpyf", 0x99, fmul),
	
	#define FP_CMP(NAME, OPC) \
	{NAME, 3, OPC, 0xff, 0, 0, 0, fcmp, {DEST_REGG, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, OPC, 0xff, 0, 1, 0, fcmp, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, OPC, 0xff, 1, 1, 0, fcmp, {DEST_REGG, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, (OPC+1), 0xff, 0, 0, 0, fcmp, {DEST1_REGB, SRC1_REGG, SRC2_REGG}},\
	{NAME, 3, (OPC+1), 0xff, 0, 1, 0, fcmp, {DEST1_REGB, SRC1_REGG, SHORT_IMM}},\
	{NAME, 3, (OPC+1), 0xff, 1, 1, 0, fcmp, {DEST1_REGB, SRC1_REGG, SHORT_IMM}}
	FP_CMP("cmpgef", 0x9a),
	FP_CMP("cmpeqf", 0x9c),
	FP_CMP("cmpgtf", 0x9e),

	#define FP_CMP_INV(NAME, OPC) \
	{NAME, 3, OPC, 0xff, 0, 0, 0, fcmp, {DEST_REGG, SRC2_REGG, SRC1_REGG}},\
	{NAME, 3, (OPC+1), 0xff, 0, 0, 0, fcmp, {DEST1_REGB, SRC2_REGG, SRC1_REGG}}
	FP_CMP_INV("cmpltf", 0x9a),
	FP_CMP_INV("cmplef", 0x9e),

	#define STORE_COND_INST(NAME, OPC) \
	{NAME, 4, OPC, 0xf8, 0, 1, 0, mem, {SRC1_REGB, SHORT_IMM, SRC1_REGG, DEST_REGG}}, \
	{NAME, 4, OPC, 0xf8, 1, 1, 0, mem, {SRC1_REGB, SHORT_IMM, SRC1_REGG, DEST_REGG}}
    MEM_LOAD_INST("ldwl", 160),
    STORE_COND_INST("stwl", 168),

	{NULL, 0, 0, 0, 0, 0, 0, 0, {{0,0,0,0}}}
};


const int rvex_num_opcodes = ARRAY_SIZE (rvex_instruction) ;


