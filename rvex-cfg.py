#!/usr/bin/env python3

import sys
import json

if __name__ == '__main__':
    
    # Check command line/print usage.
    if len(sys.argv) < 2:
        print('Usage: %s <cfg.json>' % sys.argv[0], file=sys.stderr)
        print('', file=sys.stderr)
        print('Configures the source tree in the current directory according to the given', file=sys.stderr)
        print('configuration file. Returns 0 if successful, 3 if the tool should not be', file=sys.stderr)
        print('installed, or something else (usually 1) if an error occurred.', file=sys.stderr)
        print('', file=sys.stderr)
        print('You can also run `%s -` to have the script generate default versions of the', file=sys.stderr)
        print('source files. These are the ones that should be checked into the git repos.', file=sys.stderr)
        sys.exit(2)
    
    # Load the configuration file.
    if sys.argv[1].strip() == '-':
        cfg_json = {}
    else:
        with open(sys.argv[1], 'r') as f:
            cfg_json = json.load(f)

class Config(object):
    
    def __init__(self, d, prefix=''):
        self._d = d
        self._p = prefix
        self._s = set()
        for key in d:
            if not key.startswith(prefix):
                continue
            key = key[len(prefix):].split('.')
            if len(key) == 1:
                continue
            self._s.add(key[0])
    
    def __getattr__(self, name):
        val = self._d.get(self._p + name, None)
        if val is not None:
            return val
        
        if name in self._s:
            return Config(self._d, self._p + name + '.')
        
        raise AttributeError(name)
    
    def __str__(self):
        data = ['    %s: %s\n' % (key[len(self._p):], value) for key, value in sorted(self._d.items()) if key.startswith(self._p)]
        return 'Config(\n' + ''.join(data) + ')'

def parse_cfg(cfg_json, defaults):
    """Parses the config.json contents into a Config object that is guaranteed
    to contain the keys in `defaults`. An error is printed if the given cfg_json
    dict contains non-default keys which are not present in the `defaults`
    dict, which implies that the configuration file has a higher version than
    this file and is doing something that isn't backwards compatible."""
    
    # Check that we don't have any "required" keys that we don't know about.
    for key, value in cfg_json.items():
        if value['req'] and key not in defaults:
            print('Error: found non-default key "%s", which this version of rvex-cfg.py doesn\'t know about!' % key, file=sys.stderr)
            sys.exit(1)
    
    # Build our cfg dictionary.
    cfg = {}
    for key, default in defaults.items():
        value = cfg_json.get(key, {'val': default})
        if value is None:
            print('Error: no configuration value specified for key "%s"!' % key, file=sys.stderr)
            sys.exit(1)
        cfg[key] = value['val']
    
    return Config(cfg)

def template(infname, outfname, *args, **kwargs):
    """Template engine simply using Python's str.format()."""
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            outf.write(inf.read().format(*args, **kwargs))

def template2(infname, outfname, *args, **kwargs):
    """Template engine using Python's str.format(), but with different
    characters that are more suitable for C:
     - open:   <|
     - close:  |>
     - escape: <-| to <|
               |-> to |>
    """
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            data = inf.read()
            data = data.replace('{', '{{').replace('}', '}}')
            data = data.replace('<|', '{').replace('|>', '}')
            data = data.format(*args, **kwargs)
            data = data.replace('<-|', '<|').replace('|->', '|>')
            outf.write(data)

def dont_build(s=''):
    if s:
        s = ': ' + s
    print('This tool does not need to be built%s' % s, file=sys.stderr)
    sys.exit(3)

def misconfigured(s):
    if s:
        s = ': ' + s
    print('Configuration error%s' % s, file=sys.stderr)
    sys.exit(1)

def done(s):
    if s:
        s = ': ' + s
    print('Configuration complete%s' % s, file=sys.stderr)
    sys.exit(0)

#===============================================================================
# Tool-specific from here onwards
#===============================================================================

cfg = parse_cfg(cfg_json, {
    
    # Number of lanes.
    'resources.num_lanes': 8,
    
    # Lane resources (list of boolean).
    'resources.int_mul_lanes': [True]*8,
    'resources.float_add_lanes': [False]*8,
    'resources.float_mul_lanes': [False]*8,
    'resources.float_cmp_lanes': [False]*8,
    'resources.float_i2f_lanes': [False]*8,
    'resources.float_f2i_lanes': [False]*8,
    
    # Branch unit configuration.
    'resources.bundle_align': 2,
    
    # Memory lane configuration.
    'resources.num_groups': 4,
    'resources.mem_lane': 0,
    'resources.mem_one_per_cyc': True,
    
    # Borrow configuration.
    'resources.limmh_prev': False,
    'resources.limmh_neighbor': True,
    
    # Binutils config.
    'toolchain.generic_binary': True,
    'toolchain.autosplit': True,
    
    # Memory layout.
    'memory.data_base_rvex': 0,
    'memory.data_size': 512*1024*1024,
    'memory.stack_size': 16*1024,
    'memory.heap_size': 16*1024,
    'memory.insn_base_rvex': 0,
    'memory.insn_size': 512*1024*1024,
    
})

# Generate the configuration strings.
config_str = ''
fconfig_str = ''
for lane in range(cfg.resources.num_lanes):
    config = 0
    fconfig = 0
    
    # ALU: always.
    config += 1
    
    # MUL: depends on multiplier config.
    if cfg.resources.int_mul_lanes[lane]:
        config += 2
    
    # MEM: depends on memory config.
    if cfg.resources.mem_one_per_cyc:
        if lane == cfg.resources.mem_lane:
            config += 4
    else:
        lane_in_group = lane % (cfg.resources.num_lanes // cfg.resources.num_groups)
        if lane_in_group == cfg.resources.mem_lane:
            config += 4
    
    # BR: depends on bundle alignment.
    if lane & (cfg.resources.bundle_align-1) == cfg.resources.bundle_align-1:
        config += 8
    
    # Float units: depends on float unit configs.
    if cfg.resources.float_add_lanes[lane]:
        fconfig += 1
    if cfg.resources.float_mul_lanes[lane]:
        fconfig += 2
    if cfg.resources.float_cmp_lanes[lane]:
        fconfig += 4
    if cfg.resources.float_i2f_lanes[lane]:
        fconfig += 8
    if cfg.resources.float_f2i_lanes[lane]:
        fconfig += 16
    
    key = '0123456789ABCDEFGHIJKLMNOPQRSTUV'
    config_str += key[config]
    fconfig_str += key[fconfig]

# Generate the borrow string.
borrow_str = []
for insn_lane in range(cfg.resources.num_lanes):
    limmh_lanes = []
    if cfg.resources.limmh_prev:
        if insn_lane >= 2:
            limmh_lanes.append(str(insn_lane - 2))
    if cfg.resources.limmh_neighbor:
        limmh_lanes.append(str(insn_lane ^ 1))
    borrow_str.append(','.join(limmh_lanes))
borrow_str = '.'.join(borrow_str)

# Generate tc-rvex.c.
template2(
    'gas/config/tc-rvex.c.tpl',
    'gas/config/tc-rvex.c',
    
    generic_binary      = '1' if cfg.toolchain.generic_binary else '0',
    generic_binary_flag = '-u ' if cfg.toolchain.generic_binary else '',
    
    autosplit           = '1' if cfg.toolchain.autosplit else '0',
    autosplit_flag      = ' --autosplit' if cfg.toolchain.autosplit else '',
    
    config_str          = config_str,
    fconfig_str         = fconfig_str,
    borrow_str          = borrow_str,
    
    num_lanes           = str(cfg.resources.num_lanes),
    bundle_align        = str(cfg.resources.bundle_align)
)

# Generate elf32rvex.sc (linker script).
if cfg.memory.data_base_rvex == cfg.memory.insn_base_rvex:
    data_mem_def = 'ram : ORIGIN = 0x%08X, LENGTH = %d' % (cfg.memory.data_base_rvex, cfg.memory.data_size)
    insn_mem_def = ''
    data_mem = 'ram'
    insn_mem = 'ram'
else:
    data_mem_def = 'dmem : ORIGIN = 0x%08X, LENGTH = %d' % (cfg.memory.data_base_rvex, cfg.memory.data_size)
    insn_mem_def = 'imem : ORIGIN = 0x%08X, LENGTH = %d' % (cfg.memory.insn_base_rvex, cfg.memory.insn_size)
    data_mem = 'dmem'
    insn_mem = 'imem'

template2(
    'ld/scripttempl/elf32rvex.sc.tpl',
    'ld/scripttempl/elf32rvex.sc',
    
    data_mem_def        = data_mem_def,
    insn_mem_def        = insn_mem_def,
    data_mem            = data_mem,
    insn_mem            = insn_mem,
    
    stack_size          = cfg.memory.stack_size,
    heap_size           = cfg.memory.heap_size
)
